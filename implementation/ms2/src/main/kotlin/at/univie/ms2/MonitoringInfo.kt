package at.univie.ms2

import at.univie.messagequeue.monitor.events.ConnectEvent
import at.univie.messagequeue.monitor.events.DisconnectEvent
import at.univie.messaging.monitoring.ClientDetails
import at.univie.messaging.monitoring.MonitoringData
import org.springframework.stereotype.Service

@Service
open class MonitoringInfo {
    private var exchangedMessages: Int = 0
    private var errors: Int = 0
    private var clients: MutableList<ClientDetails> = mutableListOf()

    fun getExchangedMessages(): Int {
        return exchangedMessages
    }

    fun getErrors(): Int {
        return errors
    }

    fun getConnectedClients(): List<ClientDetails> {
        return clients
    }

    fun getMonitoringData(): MonitoringData {
        return MonitoringData(
                exchangedMessages = exchangedMessages,
                errors = errors,
                connectedClients = clients
        )
    }

    open fun addExchangedMessages() {
        ++exchangedMessages
    }

    open fun addErrors() {
        ++errors
    }

    open fun addConnectedClients(event: ConnectEvent) {
        clients.add(ClientDetails(event.clientName, event.clientId, event.timestamp))
    }

    open fun removeConnectedClients(event: DisconnectEvent) {
        clients.removeIf { it.id == event.clientId }
    }
}