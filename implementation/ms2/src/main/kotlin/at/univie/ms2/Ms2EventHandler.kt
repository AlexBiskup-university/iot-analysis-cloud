package at.univie.ms2

import at.univie.messagequeue.monitor.EventHandler
import at.univie.messagequeue.monitor.events.*
import org.slf4j.Logger
import org.springframework.stereotype.Component

@Component
class Ms2EventHandler(
    private val monitoringInfo: MonitoringInfo,
    private val log: Logger
) : EventHandler {

    override fun handleEvent(event: Event) {
        log.debug("Received event: $event")
        when (event) {
            is ConnectEvent -> monitoringInfo.addConnectedClients(event)
            is DisconnectEvent -> monitoringInfo.removeConnectedClients(event)
            is ErrorEvent -> monitoringInfo.addErrors()
            is PublishEvent -> monitoringInfo.addExchangedMessages()
            else -> log.warn("Unsupported event type: ${event::class.simpleName}")
        }
    }

}
