package at.univie.ms2.configuration

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy

@Configuration
class SessionConfiguration {

    @Bean(destroyMethod = "close")
    @Lazy
    fun session(messageQueue: MessageQueue, monitor: Monitor): MonitoringSession = messageQueue.connect(monitor)
}
