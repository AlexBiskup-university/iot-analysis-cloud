package at.univie.ms2

import at.univie.messagequeue.monitor.MonitoringSession
import org.springframework.stereotype.Service
import java.util.*

@Service
open class MonitoringSessionService(
        private val session: MonitoringSession
) {
    open fun terminateClient(id: UUID) {
        session.terminate(id)
    }
}