package at.univie.ms2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["at.univie"])
class Ms2Application

fun main(args: Array<String>) {
    runApplication<Ms2Application>(*args)
}
