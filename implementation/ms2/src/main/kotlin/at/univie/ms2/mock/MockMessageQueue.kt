package at.univie.ms2.mock

import at.univie.messagequeue.Message.Priority.MEDIUM
import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import at.univie.messagequeue.monitor.events.*
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.LocalDateTime
import java.util.*
import kotlin.concurrent.thread
import kotlin.random.Random

@Configuration
class MockMessageQueueConfiguration {

    @Bean
    @ConditionalOnMissingBean
    fun mockMessageQueue() = MockMessageQueue()

}

class MockMessageQueue : MessageQueue {
    override fun connect(client: Client): ClientSession {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun connect(monitor: Monitor): MonitoringSession {
        return MockMonitoringSession(monitor)
    }
}


class MockMonitoringSession(
    private val monitor: Monitor
) : MonitoringSession {

    private val log = LoggerFactory.getLogger(this::class.java)

    init {
        thread(start = true) {
            while (true) {
                val handler = monitor.eventHandler
                val id = UUID.randomUUID()
                handler.handleEvent(ConnectEvent(id, LocalDateTime.now(), "Client${Random.nextInt()}"))
                Thread.sleep(1000)
                val topic = "/topic/${Random.nextInt()}"
                handler.handleEvent(SubscribeEvent(id, LocalDateTime.now(), SubscribeEvent.SubscriptionDetails(listOf(topic), Random.nextLong())))
                Thread.sleep(1000)
                val publishEvent = PublishEvent(id, LocalDateTime.now(), PublishEvent.MessageDetails("/topic/${Random.nextInt()}", MEDIUM))
                handler.handleEvent(publishEvent)
                Thread.sleep(1000)
                handler.handleEvent(publishEvent)
                Thread.sleep(1000)
                handler.handleEvent(publishEvent)
                Thread.sleep(2000)
                handler.handleEvent(ErrorEvent(id, LocalDateTime.now(), "Something went wrong."))
                thread(start = true) {
                    Thread.sleep(30000)
                    handler.handleEvent(DisconnectEvent(id, LocalDateTime.now()))
                }
                Thread.sleep(10000)
            }
        }
    }

    override fun close() {
        log.info("Closing session.")
    }

    override fun terminate(clientId: UUID) {
        log.info("Terminate client $clientId.")
    }

    override fun isOpen(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
