package at.univie.ms2

import at.univie.messaging.monitoring.ClientDetails
import at.univie.messaging.monitoring.MonitoringData
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/monitoring")
class MonitorRestController(
    private val monitoringSessionService: MonitoringSessionService,
    private val monitoringInfo: MonitoringInfo
) {

    @GetMapping("/exchanged")
    fun getExchanged(): Int {
        return monitoringInfo.getExchangedMessages()
    }

    @GetMapping("/errors")
    fun getErrors(): Int {
        return monitoringInfo.getErrors()
    }

    @GetMapping("/clients")
    fun getConnected(): List<ClientDetails> {
        return monitoringInfo.getConnectedClients()
    }

    @GetMapping
    fun getMonitoringData(): MonitoringData {
        return monitoringInfo.getMonitoringData()
    }

    @PostMapping("/terminate")
    fun terminateClient(@RequestBody id: UUID) {
        monitoringSessionService.terminateClient(id)
    }

}
