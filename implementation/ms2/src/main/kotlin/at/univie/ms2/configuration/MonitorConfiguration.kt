package at.univie.ms2.configuration

import at.univie.messagequeue.monitor.EventHandler
import at.univie.messagequeue.monitor.Monitor
import at.univie.ms1client.messagequeue.remote.monitor.SimpleMonitor
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MonitorConfiguration {

    @Bean
    fun monitor(eventHandler: EventHandler): Monitor {
        return SimpleMonitor(name = "MS2", eventHandler = eventHandler)
    }

}
