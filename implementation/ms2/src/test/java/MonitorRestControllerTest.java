import at.univie.messaging.monitoring.ClientDetails;
import at.univie.messaging.monitoring.MonitoringData;
import at.univie.ms2.MonitorRestController;
import at.univie.ms2.MonitoringInfo;
import at.univie.ms2.MonitoringSessionService;
import at.univie.ms2.Ms2Application;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.containsString;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes=Ms2Application.class)
@WebMvcTest(MonitorRestController.class)
public class MonitorRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MonitoringSessionService monitoringSessionService;

    @MockBean
    private MonitoringInfo monitoringInfo;

    @Test
    public void getExchanged() {
        try {
            Mockito.when(monitoringInfo.getExchangedMessages()).thenReturn(3);
            this.mockMvc
                    .perform(get("/monitoring/exchanged"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().string(containsString("3")));
        } catch (Exception e) {
            fail("No exceptions should be thrown");
        }
    }

    @Test
    public void getErrors() {
        try {
            Mockito.when(monitoringInfo.getErrors()).thenReturn(5);
            this.mockMvc
                    .perform(get("/monitoring/errors"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().string(containsString("5")));
        } catch (Exception e) {
            fail("No exceptions should be thrown");
        }
    }

    @Test
    public void getConnected() {
        try {
            List<ClientDetails> connected = new ArrayList<>();
            connected.add(new ClientDetails("specialname", UUID.randomUUID(), LocalDateTime.now()));
            Mockito.when(monitoringInfo.getConnectedClients()).thenReturn(connected);
            this.mockMvc
                    .perform(get("/monitoring/clients"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().string(containsString("specialname")));
        } catch (Exception e) {
            fail("No exceptions should be thrown");
        }
    }

    @Test
    public void getMonitoringData() {
        try {
            List<ClientDetails> connected = new ArrayList<>();
            connected.add(new ClientDetails("specialname", UUID.randomUUID(), LocalDateTime.now()));
            MonitoringData data = new MonitoringData(2, 5, connected);

            Mockito.when(monitoringInfo.getMonitoringData()).thenReturn(data);
            this.mockMvc
                    .perform(get("/monitoring"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().string(containsString("2")))
                    .andExpect(MockMvcResultMatchers.content().string(containsString("5")))
                    .andExpect(MockMvcResultMatchers.content().string(containsString("specialname")));
        } catch (Exception e) {
            fail("No exceptions should be thrown");
        }
    }
}
