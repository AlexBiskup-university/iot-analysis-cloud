package at.univie.ms2

import at.univie.messagequeue.Message
import at.univie.messagequeue.monitor.events.*
import org.junit.jupiter.api.Test

import org.mockito.Mockito
import org.slf4j.Logger
import java.time.LocalDateTime
import java.util.*

internal class Ms2EventHandlerTest {
    private val monitoringInfo = Mockito.mock(MonitoringInfo::class.java)
    private val logger = Mockito.mock(Logger::class.java)
    private val eventHandler = Ms2EventHandler(monitoringInfo, logger)

    /**
     * Test that connect event adds connected client to monitoringInfo
     */
    @Test
    fun handleConnectEvent() {
        val connectEvent = ConnectEvent(UUID.randomUUID(), LocalDateTime.now(), "1")
        eventHandler.handleEvent(connectEvent)
        Mockito.verify(monitoringInfo).addConnectedClients(connectEvent)
    }

    /**
     * Test that disconnect event removes connected client from monitoringInfo
     */
    @Test
    fun handleDisconnectEvent() {
        val disconnectEvent = DisconnectEvent(UUID.randomUUID(), LocalDateTime.now())
        eventHandler.handleEvent(disconnectEvent)
        Mockito.verify(monitoringInfo).removeConnectedClients(disconnectEvent)
    }

    /**
     * Test that publish event increases number of exchanged messages in monitoringInfo
     */
    @Test
    fun handlePublishEvent() {
        val publishEvent = PublishEvent(UUID.randomUUID(), LocalDateTime.now(), PublishEvent.MessageDetails("", Message.Priority.LOW))
        eventHandler.handleEvent(publishEvent)
        Mockito.verify(monitoringInfo).addExchangedMessages()
    }

    /**
     * Test that error event increases number of errors in monitoringInfo
     */
    @Test
    fun handleErrorEvent() {
        val errorEvent = ErrorEvent(UUID.randomUUID(), LocalDateTime.now(), "")
        eventHandler.handleEvent(errorEvent)
        Mockito.verify(monitoringInfo).addErrors()
    }

    /**
     * Test that other random event will not trigger any monitoring actions
     */
    @Test
    fun handleOtherEvent() {
        val errorEvent = UnsubscribeEvent(UUID.randomUUID(), LocalDateTime.now())
        eventHandler.handleEvent(errorEvent)
        Mockito.verifyNoInteractions(monitoringInfo)
    }
}