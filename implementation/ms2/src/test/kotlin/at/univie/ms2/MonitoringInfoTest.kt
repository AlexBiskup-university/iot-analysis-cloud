package at.univie.ms2

import at.univie.messagequeue.monitor.events.ConnectEvent
import at.univie.messagequeue.monitor.events.DisconnectEvent
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.time.LocalDateTime
import java.util.*

internal class MonitoringInfoTest {
    private val monitoringInfo = MonitoringInfo()

    /**
     * Test that getExchangedMessages() returns correct value and addExchangedMessages() increases number of exchangesMessages
     */
    @Test
    fun addExchangedMessages() {
        val beforeExchanged = monitoringInfo.getExchangedMessages()
        monitoringInfo.addExchangedMessages()
        val afterExchanged = monitoringInfo.getExchangedMessages()
        assertEquals(beforeExchanged + 1, afterExchanged)
    }

    /**
     * Test that getErrors() returns correct value and addErrors() increases number of errors
     */
    @Test
    fun addErrors() {
        val beforeErrors = monitoringInfo.getErrors()
        monitoringInfo.addErrors()
        val afterErrors  = monitoringInfo.getErrors()
        assertEquals(beforeErrors + 1, afterErrors)
    }

    /**
     * Test that getConnectedClients() returns correct list and addConnectedClients() adds a client creates from
     * ConnectEvent
     */
    @Test
    fun addConnectedClients() {
        val id = UUID.randomUUID()
        monitoringInfo.addConnectedClients(ConnectEvent(id, LocalDateTime.now(), "1"))
        val clients  = monitoringInfo.getConnectedClients()
        assertEquals("1", clients[0].name)
        assertEquals(id, clients[0].id)
    }

    /**
     * Test that removeConnectedClients() removes client with the id specified in the event
     * Test that removeConnectedClients() doesn't throw an exception if there is no client with this id
     */
    @Test
    fun removeConnectedClients() {
        val toRemoveId = UUID.randomUUID()
        monitoringInfo.addConnectedClients(ConnectEvent(toRemoveId, LocalDateTime.now(), "1"))
        monitoringInfo.addConnectedClients(ConnectEvent(UUID.randomUUID(), LocalDateTime.now(), "2"))

        monitoringInfo.removeConnectedClients(DisconnectEvent(toRemoveId, LocalDateTime.now()))
        monitoringInfo.removeConnectedClients(DisconnectEvent(UUID.randomUUID(), LocalDateTime.now()))

        val clients  = monitoringInfo.getConnectedClients()
        assertEquals("2", clients[0].name)
    }
}