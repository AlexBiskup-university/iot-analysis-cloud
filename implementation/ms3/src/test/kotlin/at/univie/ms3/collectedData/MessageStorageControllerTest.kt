package at.univie.ms3.collectedData

import at.univie.messagequeue.Message
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

internal class MessageStorageControllerTest {
    private val numberMessages = 100

    @Test
    fun storeMessage() {
        val storageController = MessageStorageController()

        for (i in 0..numberMessages) {
            storageController.storeMessage(Message("topic0", LocalDateTime.now(), Message.Priority.MEDIUM, "payload $i"))
            storageController.storeMessage(Message("topic1", LocalDateTime.now(), Message.Priority.HIGH, "playload $i"))
        }

        assertEquals(numberMessages + 1, storageController.getMessages("topic0").size)
        assertEquals(numberMessages + 1, storageController.getMessages("topic1").size)

    }


    @Test
    fun getMessages() {
        val storageController = MessageStorageController()

        for (i in 0..numberMessages) {
            storageController.storeMessage(Message("topic0", LocalDateTime.now(), Message.Priority.MEDIUM, "payload $i"))
            storageController.storeMessage(Message("topic1", LocalDateTime.now(), Message.Priority.HIGH, "playload $i"))
        }

        val listMessages = mutableListOf<Message>()

        for (i in 0..numberMessages) {
            listMessages.add(Message("topic2", LocalDateTime.now(), Message.Priority.CRITICAL, "payload $i"))
        }
        storageController.storeMessages(listMessages)

        assertNotNull(storageController.getMessages("topic0"))
        assertNotNull(storageController.getMessages("topic1"))
        assertNotNull(storageController.getMessages("topic2"))

        assertEquals(storageController.getMessages("topic10").size, 0)

        assertEquals(numberMessages+1, storageController.getMessages("topic0").size)
        assertEquals(numberMessages+1, storageController.getMessages("topic1").size)
        assertEquals(numberMessages+1, storageController.getMessages("topic2").size)
    }
}
