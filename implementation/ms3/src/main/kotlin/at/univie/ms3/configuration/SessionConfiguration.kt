package at.univie.ms3.configuration

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SessionConfiguration {

    @Bean(destroyMethod = "close")
    fun clientSession(messageQueue: MessageQueue, client: Client): ClientSession {
        return messageQueue.connect(client)
    }

}
