package at.univie.ms3

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.MessageHandler
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messaging.messages.MessageTopicCollections
import at.univie.ms1client.messagequeue.remote.configuration.MessageQueueProperties
import at.univie.ms3.collectedData.MessageStorageController
import at.univie.ms3.web.SessionManager
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.web.socket.TextMessage

@SpringBootApplication(scanBasePackages = ["at.univie"])
class Ms3Application(
    private val session: ClientSession,
    private val messageHandler: Ms3MessageHandler,
    private val messageQueueProperties: MessageQueueProperties
) {

    @EventListener
    fun onStartup(event: ApplicationStartedEvent) {
        session.subscribe(SubscriptionDetails(MessageTopicCollections.MS4_TOPICS, messageHandler, messageQueueProperties.deliveryInterval))
    }

}

fun main(args: Array<String>) {
    runApplication<Ms3Application>(*args)
}

@Component
class Ms3MessageHandler(
    private val messageStorageController: MessageStorageController,
    private val sessionManager: SessionManager,
    private val objectMapper: ObjectMapper,
    private val log: Logger
) : MessageHandler {

    private val lock = Any()

    override fun handleMessage(message: Message) {
        log.debug("Received message: $message")
        messageStorageController.storeMessage(message)

        sessionManager.getByTopic(message.topic).forEach {
            val payload = objectMapper.writeValueAsString(listOf(message))
            synchronized(lock) {
                it.sendMessage(TextMessage(payload))
            }
        }
    }
}
