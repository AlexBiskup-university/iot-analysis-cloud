package at.univie.ms3.web

import org.springframework.stereotype.Component
import org.springframework.web.socket.WebSocketSession

@Component
class SessionManager () {

    val sessionTopics: MutableMap<String, MutableList<WebSocketSession>> = mutableMapOf()

    fun addClient(topic: String, session: WebSocketSession) {
        sessionTopics.getOrPut(topic, { mutableListOf() } ).add(session)
    }

    fun getByTopic(topic: String): Collection<WebSocketSession> {
        return sessionTopics.getOrDefault(topic, emptyList())
    }

    fun remove (topic: String, session: WebSocketSession) {
        sessionTopics.get(topic)?.remove(session)
    }
}