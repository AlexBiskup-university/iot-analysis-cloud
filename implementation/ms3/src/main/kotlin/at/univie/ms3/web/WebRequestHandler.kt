package at.univie.ms3.web

import at.univie.messaging.messages.MessageTopics
import at.univie.ms3.collectedData.MessageStorageController
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler


abstract class AbstractSubscriptionWebSocketHandler(
    private val subscriptionTopic: String,
    private val log: Logger,
    private val sessionManager: SessionManager,
    private val messageStorageController: MessageStorageController,
    private val objectMapper: ObjectMapper
) : TextWebSocketHandler() {

    override fun afterConnectionEstablished(session: WebSocketSession) {
        log.debug("Client connected: $subscriptionTopic")
        sessionManager.addClient(subscriptionTopic, session)
        val messages = messageStorageController.getMessages(subscriptionTopic)
        val payload = objectMapper.writeValueAsString(messages)
        val message = TextMessage(payload)
        session.sendMessage(message)
    }

    override fun afterConnectionClosed(session: WebSocketSession, status: CloseStatus) {
        sessionManager.remove(subscriptionTopic, session)
        log.debug("Client disconnected: $subscriptionTopic")
    }

}

@Component
class TemperatureDataWebSocketHandler(
    messageStorageController: MessageStorageController,
    objectMapper: ObjectMapper,
    sessionManager: SessionManager,
    log: Logger
) : AbstractSubscriptionWebSocketHandler(MessageTopics.SENSOR_DATA_TEMPERATURE, log, sessionManager, messageStorageController, objectMapper)

@Component
class AnalyzedTemperatureDataWebSocketHandler(
    messageStorageController: MessageStorageController,
    objectMapper: ObjectMapper,
    sessionManager: SessionManager,
    log: Logger
) : AbstractSubscriptionWebSocketHandler(MessageTopics.ANALYZED_TEMPERATURE_DATA, log, sessionManager, messageStorageController, objectMapper)

@Component
class LightIntensityDataWebSocketHandler(
    messageStorageController: MessageStorageController,
    objectMapper: ObjectMapper,
    sessionManager: SessionManager,
    log: Logger
) : AbstractSubscriptionWebSocketHandler(MessageTopics.SENSOR_DATA_LIGHT_INTENSITY, log, sessionManager, messageStorageController, objectMapper)

@Component
class AnalyzedLightIntensityDataWebSocketHandler(
    messageStorageController: MessageStorageController,
    objectMapper: ObjectMapper,
    sessionManager: SessionManager,
    log: Logger
) : AbstractSubscriptionWebSocketHandler(MessageTopics.ANALYZED_LIGHT_INTENSITY_DATA, log, sessionManager, messageStorageController, objectMapper)
