package at.univie.ms3.mock

import at.univie.messagequeue.Message
import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import at.univie.messaging.messages.MessageTopics
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.lang.Thread.sleep
import java.time.LocalDateTime
import kotlin.concurrent.thread
import kotlin.random.Random

@Configuration
class Config {
    @ConditionalOnMissingBean
    @Bean
    fun mockMessageQueue(objectMapper: ObjectMapper): MessageQueue = MockMessageQueue(objectMapper)
}

class MockMessageQueue(
    private val objectMapper: ObjectMapper
) : MessageQueue {

    override fun connect(client: Client): ClientSession {
        return MockClientSession(objectMapper)
    }

    override fun connect(monitor: Monitor): MonitoringSession {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

class MockClientSession(
    private val objectMapper: ObjectMapper
) : ClientSession {

    private var isOpen: Boolean = true
    private var deliveryInterval: Long = 3000

    private var subscription: SubscriptionDetails? = null

    override fun isOpen(): Boolean {
        subscription = null
        return isOpen
    }

    override fun close() {
        isOpen = false
    }

    override fun publish(message: Message) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun subscribe(details: SubscriptionDetails) {
        subscription = details

        thread(true) {
            while (true) {
                val handler = subscription?.messageHandler
                val temperaturePayload = AnalyzedData(timestamp = LocalDateTime.now(), predictedMeanValue = Random.nextDouble())
                val temperatureMessage = Message(
                    topic = MessageTopics.ANALYZED_TEMPERATURE_DATA,
                    payload = objectMapper.writeValueAsString(temperaturePayload),
                    priority = Message.Priority.HIGH,
                    timestamp = LocalDateTime.now()
                )
                handler?.handleMessage(temperatureMessage)
                val lightPayload = AnalyzedData(timestamp = LocalDateTime.now(), predictedMeanValue = Random.nextDouble())
                val lightMessage = Message(
                    topic = MessageTopics.ANALYZED_LIGHT_INTENSITY_DATA,
                    payload = objectMapper.writeValueAsString(lightPayload),
                    priority = Message.Priority.LOW,
                    timestamp = LocalDateTime.now()
                )
                handler?.handleMessage(lightMessage)
                sleep(deliveryInterval)
            }
        }
    }

    override fun unsubscribe() {
        subscription = null
    }

    override fun updateDeliveryInterval(interval: Long) {
        deliveryInterval = interval
    }

}

private class AnalyzedData(val predictedMeanValue: Double, val timestamp: LocalDateTime)

