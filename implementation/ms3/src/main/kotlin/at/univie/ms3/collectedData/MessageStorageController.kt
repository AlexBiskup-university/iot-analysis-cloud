package at.univie.ms3.collectedData

import at.univie.messagequeue.Message
import org.springframework.stereotype.Component

@Component
class MessageStorageController {

    private val topicToMessages: MutableMap<String, MutableList<Message>> = mutableMapOf()

    fun storeMessage(message: Message) {
        topicToMessages.getOrPut(message.topic, { mutableListOf() }).add(message)
    }

    fun storeMessages(messages: List<Message>) {
            for (m in messages) storeMessage(m)
    }

    fun getMessages(topic: String): Collection<Message> {
        return topicToMessages.getOrDefault(topic, emptyList())
    }

}

