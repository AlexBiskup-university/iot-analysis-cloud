package at.univie.ms3.configuration

import at.univie.messaging.messages.MessageTopics
import at.univie.ms3.web.*
import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry

@Configuration
@EnableWebSocket
class WebSocketConfiguration(
    private val temperatureDataWebSocketHandler: TemperatureDataWebSocketHandler,
    private val analyzedTemperatureDataWebSocketHandler: AnalyzedTemperatureDataWebSocketHandler,
    private val lightIntensityDataWebSocketHandler: LightIntensityDataWebSocketHandler,
    private val analyzedLightIntensityDataWebSocketHandler: AnalyzedLightIntensityDataWebSocketHandler

): WebSocketConfigurer {

    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(temperatureDataWebSocketHandler, MessageTopics.SENSOR_DATA_TEMPERATURE).setAllowedOrigins("*")
        registry.addHandler(analyzedTemperatureDataWebSocketHandler, MessageTopics.ANALYZED_TEMPERATURE_DATA).setAllowedOrigins("*")
        registry.addHandler(lightIntensityDataWebSocketHandler, MessageTopics.SENSOR_DATA_LIGHT_INTENSITY).setAllowedOrigins("*")
        registry.addHandler(analyzedLightIntensityDataWebSocketHandler, MessageTopics.ANALYZED_LIGHT_INTENSITY_DATA).setAllowedOrigins("*")
    }
}
