package at.univie.ms4.machinelearning

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.ClientSession
import at.univie.messaging.messages.DataObject
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.LocalDateTime.now

/*
Using temporal difference learning(TDL) for calculating values

Each Entity has a state, initialized by 0
so first state will set the intitial state(first value)

Every getDiscountRate() call will reduce discount by -0.01
the state value gets changed according to the reward function
 */

class TDL(
    private val topic: String,
    private val message: DataObject,
    private val session: ClientSession,
    private val machineLearningRating: MachineLearningRating,
    private val objectMapper: ObjectMapper
) : Runnable {

    private val alpha = 0.025                                       // learning rate between [0, 1]
    private val discount = 0.01                                     // learning rate decreases every step by this value

    override fun run() {
        val rating = machineLearningRating.getRating(topic)

        val newRating = update(rating, message.getData())

        machineLearningRating.setRating(topic, newRating)

        publishMachineLearningValue(newRating)
    }

    private class AnalyzedData(val predictedMeanValue: Double, val timestamp: LocalDateTime)

    private fun publishMachineLearningValue(newRating: State) {
        session.publish(
            Message(
                "$topic/analyzed",
                now(),
                Message.Priority.MEDIUM,
                objectMapper.writeValueAsString(AnalyzedData(newRating.state, now()))
            )
        )
    }

    private fun update(entity: State, state: Double): State {        // state = message value
        val previousState: Double = entity.state
        val gamma: Double = entity.getDiscountRate(discount)        // discount rate between [0, 1]

        if (previousState == 0.0) entity.state = state              // s0 = s
        else {
            val action = previousState + alpha * (reward(gamma * previousState, state) + gamma * state - previousState)
            entity.state = action
        }

        return entity
    }

    private fun reward(previousState: Double, rating: Double): Double {
        return -(previousState - rating)
    }
}

class State(var state: Double = 0.0, private var discountRate: Double = 0.025) {

    fun getDiscountRate(discount: Double): Double {
        if (discount > 0.01) {
            discountRate -= discount
        }
        return discountRate
    }
}

@Component
class MachineLearningRating {
    private val state: MutableMap<String, State> = mutableMapOf()

    @Synchronized
    fun getRating(topic: String): State {
        return state.getOrDefault(topic, State())
    }

    @Synchronized
    fun setRating(topic: String, state: State) {
        this.state[topic] = state
    }
}
