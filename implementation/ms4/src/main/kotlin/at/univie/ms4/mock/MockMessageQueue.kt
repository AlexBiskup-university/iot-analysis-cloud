package at.univie.ms4.mock

import at.univie.messagequeue.Message
import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import at.univie.messaging.messages.LightIntensityData
import at.univie.messaging.messages.MessageTopics
import at.univie.messaging.messages.TemperatureData
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.lang.Thread.sleep
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.concurrent.thread
import kotlin.random.Random.Default.nextDouble

@Configuration
class Config {
    @ConditionalOnMissingBean
    @Bean
    fun mockMessageQueue(objectMapper: ObjectMapper): MessageQueue = MockMessageQueue(objectMapper)
}

class MockMessageQueue(
    private val objectMapper: ObjectMapper
) : MessageQueue {
    override fun connect(client: Client): ClientSession {
        return MockClientSession(objectMapper)
    }

    override fun connect(monitor: Monitor): MonitoringSession {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class MockClientSession(
    private val objectMapper: ObjectMapper

) : ClientSession {

    private val log = LoggerFactory.getLogger(this::class.java)
    private var isOpen: Boolean = true
    private var deliveryInterval: Long = 3000

    private var subscription: SubscriptionDetails? = null

    override fun isOpen(): Boolean {
        subscription = null
        return isOpen
    }

    override fun close() {
        isOpen = false
    }

    override fun publish(message: Message) {
        log.info("Publish message: $message")
    }

    override fun subscribe(details: SubscriptionDetails) {
        subscription = details
        thread(true) {
            while (true) {
                val handler = subscription?.messageHandler
                val temperaturePayload = TemperatureData(date = LocalDate.now(), temperature = nextDouble())
                val temperatureMessage = Message(
                    topic = MessageTopics.SENSOR_DATA_TEMPERATURE,
                    payload = objectMapper.writeValueAsString(temperaturePayload),
                    priority = Message.Priority.MEDIUM,
                    timestamp = LocalDateTime.now()
                )
                handler?.handleMessage(temperatureMessage)
                sleep(deliveryInterval)
                val lightPayload = LightIntensityData(date = LocalDate.now(), lightIntensity = nextDouble())
                val lightMessage = Message(
                    topic = MessageTopics.SENSOR_DATA_LIGHT_INTENSITY,
                    payload = objectMapper.writeValueAsString(lightPayload),
                    priority = Message.Priority.MEDIUM,
                    timestamp = LocalDateTime.now()
                )
                handler?.handleMessage(lightMessage)
                sleep(deliveryInterval)


            }
        }
    }

    override fun unsubscribe() {
        subscription = null
    }

    override fun updateDeliveryInterval(interval: Long) {
        deliveryInterval = interval
    }

}

