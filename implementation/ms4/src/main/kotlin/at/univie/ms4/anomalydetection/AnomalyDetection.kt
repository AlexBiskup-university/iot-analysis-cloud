package at.univie.ms4.anomalydetection

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.ClientSession
import at.univie.messaging.messages.DataObject
import org.springframework.stereotype.Component
import java.lang.NullPointerException
import java.sql.Timestamp
import java.time.LocalDateTime

/* determining of unexpected/unlikely behavior is taking place
   (for at least three different scenarios) and publishes a warning message   */

class AnomalyDetection(
    private val topic: String,
    private val message: DataObject,
    private val session: ClientSession,
    private val previousMessage: PreviousMessage
) : Runnable {
    override fun run() {
        analyze(message)
    }

    private fun analyze(message: DataObject) { // analyze the incoming data
        val prevMessage = previousMessage.getPreviousMessage(topic)

        try {
            reasonable(message.getData())
            difference(message.getData(), prevMessage?.first?.getData())
            delay(prevMessage?.second)

        } catch (e: Exception) {
                publishWarning(e.message.toString())


        } finally {
            previousMessage.storePreviousMessage(topic, message)
        }

    }

    private fun publishWarning(warning: String) { // publishes a warning message to the MQ
        session.publish(Message("$topic/warning", LocalDateTime.now(), Message.Priority.CRITICAL, warning))
    }

    private fun reasonable(value: Double) {
        if (value < 0) {
            throw Exception("Value is not reasonable. $value below zero")
        }
    }

    private fun difference(currentValue: Double, previousValue: Double?) {
        if (previousValue!= null) {
            if (currentValue > (previousValue) * 2) {
                throw Exception("Current value is too high. current: $currentValue previous: $previousValue")
            }
            if (currentValue < (previousValue * 0.05)) {
                throw Exception("Current value is too low. current: $currentValue previous: $previousValue")
            }
        }

    }

    private fun delay(timestamp: Timestamp?) {
        if (timestamp != null) {
            if (timestamp.after(Timestamp(System.currentTimeMillis() + DELAY))) {
                throw Exception("Time interval since last message is too high.")
            }
        }
    }

    companion object {
        private const val DELAY = 5000
    }
}

@Component
data class PreviousMessage(
    private var previousStoredMessage: MutableMap<String, Pair<DataObject, Timestamp>> = mutableMapOf()
) {
    @Synchronized
    fun getPreviousMessage(topic: String): Pair<DataObject, Timestamp>? {
        return previousStoredMessage[topic]
    }

    @Synchronized
    fun storePreviousMessage(topic: String, payload: DataObject) {
        previousStoredMessage[topic] = Pair(payload, Timestamp(System.currentTimeMillis()))
    }
}
