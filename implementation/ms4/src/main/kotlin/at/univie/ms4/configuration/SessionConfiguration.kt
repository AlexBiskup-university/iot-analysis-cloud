package at.univie.ms4.configuration

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy

@Configuration
class SessionConfiguration {

    @Bean(destroyMethod = "close")
    @Lazy
    fun clientSession(messageQueue: MessageQueue, client: Client): ClientSession {
        return messageQueue.connect(client)
    }

}
