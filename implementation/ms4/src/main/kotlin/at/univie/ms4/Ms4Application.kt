package at.univie.ms4

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.MessageHandler
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messaging.messages.LightIntensityData
import at.univie.messaging.messages.MessageTopicCollections
import at.univie.messaging.messages.MessageTopics
import at.univie.messaging.messages.TemperatureData
import at.univie.ms1client.messagequeue.remote.configuration.MessageQueueProperties
import at.univie.ms4.anomalydetection.AnomalyDetection
import at.univie.ms4.anomalydetection.PreviousMessage
import at.univie.ms4.machinelearning.MachineLearningRating
import at.univie.ms4.machinelearning.TDL
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import java.util.concurrent.Executors


@SpringBootApplication(scanBasePackages = ["at.univie"])
class Ms4Application(
    private val clientSession: ClientSession,
    private val messageHandler: MessageHandler,
    private val messageQueueProperties: MessageQueueProperties
) {
    @EventListener
    fun onStartup(event: ApplicationStartedEvent?) {
        clientSession.subscribe(SubscriptionDetails(MessageTopicCollections.MS5_TOPICS, messageHandler, messageQueueProperties.deliveryInterval))
    }
}

fun main(args: Array<String>) {
    runApplication<Ms4Application>(*args)
}

@Service
class Ms4MessageHandler(
    private val session: ClientSession,
    private val previousMessage: PreviousMessage,
    private val machineLearningRating: MachineLearningRating,
    private val objectMapper: ObjectMapper,
    private val log: Logger
) : MessageHandler {
    private val executor = Executors.newCachedThreadPool()

    override fun handleMessage(message: Message) {
        log.debug("Received message: $message")
        val topic = message.topic

        val payload = when (message.topic) {
            MessageTopics.SENSOR_DATA_TEMPERATURE -> objectMapper.readValue(message.payload, TemperatureData::class.java)
            MessageTopics.SENSOR_DATA_LIGHT_INTENSITY -> objectMapper.readValue(message.payload, LightIntensityData::class.java)
            else -> log.warn("Received unknown message topic: $topic").run { return }
        }

        executor.execute(AnomalyDetection(topic, payload, session, previousMessage))
        executor.execute(TDL(topic, payload, session, machineLearningRating, objectMapper))
    }
}
