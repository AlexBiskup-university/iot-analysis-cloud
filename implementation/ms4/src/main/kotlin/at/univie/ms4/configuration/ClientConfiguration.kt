package at.univie.ms4.configuration

import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.TerminationHandler
import at.univie.ms1client.messagequeue.remote.client.SimpleClient
import at.univie.ms1client.messagequeue.remote.configuration.MessageQueueProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ClientConfiguration {

    @Bean
    fun client(terminationHandler: TerminationHandler, messageQueueProperties: MessageQueueProperties): Client {
        return SimpleClient(messageQueueProperties.clientName, terminationHandler)
    }
}
