package at.univie.ms4.anomalydetection

import at.univie.messaging.messages.TemperatureData
import at.univie.ms4.mock.MockClientSession
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDate

internal class AnomalyDetectionTest {

    @Test
    fun run() {

        val previousMessage = PreviousMessage()
        val objectMapper = ObjectMapper()
        val session = MockClientSession(objectMapper)

        val topic = "/temperatur"

        val message = TemperatureData(LocalDate.now(), temperature = 12.0)
        assertEquals(message.getData(), 12.0)

        previousMessage.storePreviousMessage(topic, message)
        var prevMessage = previousMessage.getPreviousMessage("/temperatur")?.first

        assertEquals(message, prevMessage)


        val newMessage = TemperatureData(LocalDate.now(), 30.0)

        var anomalyDetection = AnomalyDetection("/temperatur", newMessage, MockClientSession(objectMapper), previousMessage)

    }
}