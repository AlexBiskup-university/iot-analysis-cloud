package at.univie.ms4.machinelearning

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.boot.test.context.SpringBootTest

class MachineLearningRatingTest {

    @Test
    fun getRating() {
        val machineLearningRating = MachineLearningRating()

        val rating = machineLearningRating.getRating("topic")

        assertEquals(State().state, rating.state)
        assertEquals(State().getDiscountRate(0.01), rating.getDiscountRate(0.01))

    }

    @Test
    fun setRating() {
        val machineLearningRating = MachineLearningRating()

        val state = State()
        machineLearningRating.setRating("topic", state)

        assertEquals(state, machineLearningRating.getRating("topic"))

    }
}