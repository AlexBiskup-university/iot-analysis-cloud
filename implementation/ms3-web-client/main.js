const Topic = Object.freeze({
  'TEMPERATURE': {
    name: 'Temperature',
    path: '/topic/sensor-data/temperature',
    extract: (payload) => ({data: payload.temperature, label: payload.date})
  },
  'LIGHT_INTENSITY': {
    name: 'Light intensity',
    path: '/topic/sensor-data/light-intensity',
    extract: (payload) => ({data: payload.lightIntensity, label: payload.date})
  },
  'TEMPERATURE_ANALYZED': {
    name: 'Temperature analyzed',
    path: '/topic/sensor-data/temperature/analyzed',
    extract: (payload) => ({
      data: payload.predictedMeanValue,
      label: payload.timestamp
    })
  },
  'LIGHT_INTENSITY_ANALYZED': {
    name: 'Light intensity analyzed',
    path: '/topic/sensor-data/light-intensity/analyzed',
    extract: (payload) => ({
      data: payload.predictedMeanValue,
      label: payload.timestamp
    })
  },
});

const state = {
  selectedTopic: Topic.TEMPERATURE,
  serverUrl: 'localhost:8083',
};

let webSocket = {
  readyState: WebSocket.CLOSED,
};
let dataGraph;

function updateUrl(serverUrl) {
  state.serverUrl = serverUrl;
  reconnect();
}

function isConnected() {
  return webSocket.readyState === WebSocket.OPEN;
}

function reconnect() {
  disconnect(connect);
}

function connect() {
  if (isConnected()) {
    reconnect();
    return;
  }
  const url = `ws://${state.serverUrl}${state.selectedTopic.path}`;
  console.log(`Connect to ${url}`);
  webSocket = new WebSocket(url);
  webSocket.onmessage = handleMessage;
  initializeGraph();
}

function disconnect(callback) {
  if (!isConnected()) {
    callback();
    return;
  }
  console.log('Disconnect');
  webSocket.onclose = callback;
  webSocket.close();
  clearGraph();
}

function updateTopic() {
  const topicKey = document.getElementById('topic-select').value;
  const updatedTopic = Topic[topicKey];
  if (state.selectedTopic === updatedTopic) {
    return
  }
  state.selectedTopic = updatedTopic;
  reconnect();
}

function handleMessage({data}) {
  const messages = JSON.parse(data);
  messages.forEach(message => {
    const payload = JSON.parse(message.payload);
    const {label, data} = state.selectedTopic.extract(payload);
    addGraphData(label, data);
  })
}

function initializeGraph() {
  dataGraph = createGraph('data-graph', state.selectedTopic.name);
}

function createGraph(containerId, label) {
  const container = document.getElementById(containerId).getContext('2d');
  return new Chart(container, {
    type: 'line',
    data: {
      labels: [],
      datasets: [{
        label: label,
        data: [],
        borderWidth: 1
      }]
    },
  });
}

function clearGraph() {
  dataGraph.data.labels = [];
  dataGraph.data.datasets.forEach((dataset) => {
    dataset.data = []
  });
  dataGraph.update();
}

function addGraphData(label, data) {
  dataGraph.data.labels.push(label);
  dataGraph.data.datasets.forEach((dataset) => {
    dataset.data.push(data);
  });
  dataGraph.update();
}
