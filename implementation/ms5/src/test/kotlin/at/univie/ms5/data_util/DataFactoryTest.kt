package at.univie.ms5.data_util

import at.univie.messaging.messages.LightIntensityData
import at.univie.messaging.messages.MessageTopics
import at.univie.messaging.messages.TemperatureData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal class DataFactoryTest {
    private val lightDataFactory = LightIntensityDataFactory()
    private val tempDataFactory = TemperatureDataFactory()

    /**
     * Test that DataFactories return correct DataObjects with provided data
     */
    @Test
    fun getDataObjectFromDateAndData() {
        val date = LocalDate.parse("01-01-18 06:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"))
        val light = 500.0
        val temperature = 23.0

        val lightData = lightDataFactory.getDataObject(date, light)
        val tempData = tempDataFactory.getDataObject(date, temperature)

        assertEquals(date, lightData.date)
        assertEquals(date, tempData.date)

        assertEquals(light, lightData.getData())
        assertEquals(temperature, tempData.getData())

        assertTrue(lightData is LightIntensityData)
        assertTrue(tempData is TemperatureData)
    }

    /**
     * Test that DataFactories return correct DataObjects with provided data
     */
    @Test
    fun getDataObjectFromRow() {
        val dataRow = FileReader.DataRow(
                0,
                "S1",
                LocalDate.parse("01-01-18 06:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss")),
                20.0,
                500.0)

        val lightData = lightDataFactory.getDataObject(dataRow)
        val tempData = tempDataFactory.getDataObject(dataRow)

        assertEquals(dataRow.date, lightData.date)
        assertEquals(dataRow.date, tempData.date)

        assertEquals(dataRow.lightIntensity, lightData.getData())
        assertEquals(dataRow.temperature, tempData.getData())

        assertTrue(lightData is LightIntensityData)
        assertTrue(tempData is TemperatureData)
    }

    /**
     * Test that DataFactories return correct topic depending on the DataFactory type
     */
    @Test
    fun getTopic() {
        assertTrue(lightDataFactory.getTopic() == MessageTopics.SENSOR_DATA_LIGHT_INTENSITY)
        assertTrue(tempDataFactory.getTopic() == MessageTopics.SENSOR_DATA_TEMPERATURE)
    }
}
