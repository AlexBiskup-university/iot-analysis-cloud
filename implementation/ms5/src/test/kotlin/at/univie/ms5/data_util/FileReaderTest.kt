package at.univie.ms5.data_util

import at.univie.ms5.configuration.properties.DataProperties
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.slf4j.Logger
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.ResourceLoader
import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal class FileReaderTest {
    /**
     * Test that if file is not available, FileReader returns empty list
     * and doesn't throw an exception
     */
    @Test
    fun getDataRowsNoFile() {
        val appProperties = Mockito.mock(DataProperties::class.java)
        Mockito.`when`(appProperties.filePath).thenReturn("invalid")

        val fileReader = FileReader(appProperties, Mockito.mock(ResourceLoader::class.java), Mockito.mock(Logger::class.java))

        assertTrue(fileReader.getDataRows().isEmpty())
    }

    /**
     * Test that if file is available, rows are read and parsed into DataRow objects
     * Test that if a line couldn't be parsed, it is just skipped and FileReader still returns list of DataRows
     */
    @Test
    fun getDataRowsFilePresent() {
        val appProperties = Mockito.mock(DataProperties::class.java)
        val resourceLoader = Mockito.mock(ResourceLoader::class.java)
        Mockito.`when`(appProperties.filePath).thenReturn("classpath:files/test_data.csv")
        Mockito.`when`(resourceLoader.getResource("classpath:files/test_data.csv")).thenReturn(ClassPathResource("files/test_data.csv"))

        val fileReader = FileReader(appProperties, resourceLoader, Mockito.mock(Logger::class.java))
        val dataRows = fileReader.getDataRows()
        val expectedFirst = FileReader.DataRow(
            1,
            "S1_2017",
            LocalDate.parse("06-01-17 06:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss")),
            23.388,
            0.0
        )

        assertEquals(2, dataRows.size)
        assertEquals(expectedFirst, dataRows.first())
    }
}
