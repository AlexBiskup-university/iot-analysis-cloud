package at.univie.ms5

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.MessageHandler
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messaging.messages.MessageTopics.SENSOR_DATA_TEMPERATURE
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertTimeoutPreemptively
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.time.Duration

@SpringBootTest
@ActiveProfiles("test")
class Ms5ApplicationTests {

    @Autowired
    private lateinit var session: ClientSession

    @Test
    fun `test sending messages to message queue`() {
        val receivedMessages = mutableListOf<Message>()

        val handler = object : MessageHandler {
            override fun handleMessage(message: Message) {
                receivedMessages.add(message)
            }
        }

        session.subscribe(SubscriptionDetails(listOf(SENSOR_DATA_TEMPERATURE), handler, 0))

        assertTimeoutPreemptively(Duration.ofSeconds(15), message = "No messages have been received in time") {
            while (receivedMessages.isEmpty()) Thread.sleep(2)
            Thread.sleep(5)
            assertTrue(receivedMessages.isNotEmpty())
        }
    }

}
