package at.univie.ms5.data_util

import at.univie.messaging.messages.LightIntensityData
import at.univie.messaging.messages.MessageTopics
import at.univie.messaging.messages.TemperatureData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal class DataHandlerTest {
    private val dataFactory = Mockito.mock(DataFactory::class.java)
    private val fileReader = Mockito.mock(FileReader::class.java)
    private val dataHandler = DataHandler(dataFactory, fileReader)

    /**
     * Test that getDataObjects() converts dataRows provided by fileReader
     * to DataObjects and returns fogged data.
     */
    @Test
    fun getDataObjects() {
        val dataRowOne = FileReader.DataRow(
                0,
                "S1",
                LocalDate.parse("01-01-18 06:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss")),
                20.0,
                500.0)
        val dataRowTwo = FileReader.DataRow(
                0,
                "S1",
                LocalDate.parse("01-01-18 07:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss")),
                30.0,
                600.0)

        val expectedTempData = TemperatureData(LocalDate.parse("01-01-18", DateTimeFormatter.ofPattern("dd-MM-yy")), 25.0)

        val dataRows = listOf(dataRowOne, dataRowTwo)

        Mockito.`when`(fileReader.getDataRows()).thenReturn(dataRows)
        Mockito.`when`(dataFactory.getDataObject(dataRowOne)).thenReturn(TemperatureData(dataRowOne.date, dataRowOne.temperature))
        Mockito.`when`(dataFactory.getDataObject(dataRowTwo)).thenReturn(TemperatureData(dataRowTwo.date, dataRowTwo.temperature))
        Mockito.`when`(dataFactory.getDataObject(expectedTempData.date, expectedTempData.getData())).thenReturn(expectedTempData)

        val dataObject = dataHandler.getDataObjects().first()

        assertEquals(expectedTempData.date, dataObject.date)
        assertEquals(expectedTempData.getData(), dataObject.getData())
    }

    /**
     * Test that fogData() averages data values by date
     */
    @Test
    fun fogData() {
        val dayOneData = LightIntensityData(
                LocalDate.parse("01-01-18 06:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss")),
                100.0)
        val dayTwoData1 = LightIntensityData(
                LocalDate.parse("01-01-19 06:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss")),
                200.0)
        val dayTwoData2 = LightIntensityData(
                LocalDate.parse("01-01-19 07:00:00", DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss")),
                300.0)
        val dayTwoData = LightIntensityData(
                LocalDate.parse("01-01-19", DateTimeFormatter.ofPattern("dd-MM-yy")),
                (dayTwoData1.getData() + dayTwoData2.getData())/2)

        Mockito.`when`(dataFactory.getDataObject(dayOneData.date, dayOneData.getData())).thenReturn(dayOneData)
        Mockito.`when`(dataFactory.getDataObject(dayTwoData1.date, dayTwoData1.getData())).thenReturn(dayTwoData1)
        Mockito.`when`(dataFactory.getDataObject(dayTwoData2.date, dayTwoData2.getData())).thenReturn(dayTwoData2)
        Mockito.`when`(dataFactory.getDataObject(dayTwoData.date, dayTwoData.getData())).thenReturn(dayTwoData)

        val data = listOf(dayOneData, dayTwoData1, dayTwoData2)
        val foggedData = dataHandler.fogData(data)

        val foggedDayOne = foggedData.first { it.date.year == 2018 }
        val foggedDayTwo = foggedData.first { it.date.year == 2019 }

        assertEquals(foggedDayOne.getData(), dayOneData.getData())
        assertEquals(foggedDayTwo.getData(), dayTwoData.getData())
        assertEquals(foggedDayTwo.date, dayTwoData.date)
    }

    /**
     * Test that getTopic() returns the same topic as dataFactory does
     */
    @Test
    fun getTopic() {
        Mockito.`when`(dataFactory.getTopic()).thenReturn(MessageTopics.SENSOR_DATA_LIGHT_INTENSITY)
        assertEquals(dataFactory.getTopic(), dataHandler.getTopic())
    }
}
