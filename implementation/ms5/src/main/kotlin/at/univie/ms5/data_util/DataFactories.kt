package at.univie.ms5.data_util

import at.univie.messaging.messages.DataObject
import at.univie.messaging.messages.LightIntensityData
import at.univie.messaging.messages.MessageTopics
import at.univie.messaging.messages.TemperatureData
import java.time.LocalDate

interface DataFactory {
    fun getDataObject(date: LocalDate, data: Double): DataObject
    fun getDataObject(dataRow: FileReader.DataRow): DataObject
    fun getTopic(): String
}

class LightIntensityDataFactory : DataFactory {
    override fun getDataObject(date: LocalDate, data: Double): DataObject {
        return LightIntensityData(date, data)
    }

    override fun getDataObject(dataRow: FileReader.DataRow): DataObject {
        return LightIntensityData(dataRow.date, dataRow.lightIntensity)
    }

    override fun getTopic(): String = MessageTopics.SENSOR_DATA_LIGHT_INTENSITY

}

class TemperatureDataFactory : DataFactory {
    override fun getDataObject(date: LocalDate, data: Double): DataObject {
        return TemperatureData(date, data)
    }

    override fun getDataObject(dataRow: FileReader.DataRow): DataObject {
        return TemperatureData(dataRow.date, dataRow.temperature)
    }

    override fun getTopic(): String = MessageTopics.SENSOR_DATA_TEMPERATURE

}