package at.univie.ms5

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.ClientSession
import at.univie.ms5.configuration.properties.DataProperties
import at.univie.ms5.data_util.DataHandler
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener
import java.time.LocalDateTime.now
import java.util.concurrent.Executor

@SpringBootApplication(scanBasePackages = ["at.univie"])
class Ms5Application(
    private val session: ClientSession,
    private val dataHandler: DataHandler,
    private val objectMapper: ObjectMapper,
    private val executor: Executor,
    private val log: Logger,
    private val dataProperties: DataProperties
) {

    @EventListener
    fun onStartup(event: ApplicationStartedEvent) {
        executor.execute {
            sendData()
        }
    }

    private fun sendData() {
        val dataObjects = dataHandler.getDataObjects()
        log.info("Starting to send messages.")
        for (dataObject in dataObjects) {
            val message = Message(dataHandler.getTopic(), now(), dataProperties.messagePriority, objectMapper.writeValueAsString(dataObject))
            session.publish(message)
            log.debug("Message published: $message")
            Thread.sleep(dataProperties.publishingInterval)
        }
        log.info("Finished sending ${dataObjects.size} messages.")
    }
}

fun main(args: Array<String>) {
    runApplication<Ms5Application>(*args)
}
