package at.univie.ms5.configuration

import at.univie.ms5.data_util.DataFactory
import at.univie.ms5.data_util.LightIntensityDataFactory
import at.univie.ms5.data_util.TemperatureDataFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DataFactoryConfiguration {

    @Bean
    @ConditionalOnProperty(name = ["data.type"], havingValue = "LIGHT_INTENSITY", matchIfMissing = true)
    fun lightIntensityDataFactory(): DataFactory {
        return LightIntensityDataFactory()
    }

    @Bean
    @ConditionalOnProperty(name = ["data.type"], havingValue = "TEMPERATURE", matchIfMissing = false)
    fun temperatureDataFactory(): DataFactory {
        return TemperatureDataFactory()
    }

}
