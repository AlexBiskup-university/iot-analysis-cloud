package at.univie.ms5.configuration

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy

@Configuration
class SessionConfiguration {

    @Bean(destroyMethod = "close")
    @Lazy
    fun session(messageQueue: MessageQueue, client: Client): ClientSession = messageQueue.connect(client)

}
