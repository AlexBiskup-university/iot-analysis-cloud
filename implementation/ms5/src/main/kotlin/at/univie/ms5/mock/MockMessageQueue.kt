package at.univie.ms5.mock

import at.univie.messagequeue.Message
import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Config {
    @ConditionalOnMissingBean
    @Bean
    fun mockMessageQueue(): MessageQueue = MockMessageQueue()
}

class MockMessageQueue : MessageQueue {
    override fun connect(client: Client): ClientSession {
        return MockClientSession()
    }

    override fun connect(monitor: Monitor): MonitoringSession {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class MockClientSession : ClientSession {

    private val log = LoggerFactory.getLogger(this::class.java)
    private var isOpen: Boolean = true

    private val subscriptions = mutableMapOf<String, SubscriptionDetails>()

    override fun isOpen(): Boolean {
        return isOpen
    }

    override fun close() {
        isOpen = false
    }

    override fun publish(message: Message) {
        require(isOpen) { "Session closed." }
        Thread.sleep(367)
        subscriptions[message.topic]?.messageHandler?.handleMessage(message)
        log.info("Published message: $message")
    }

    override fun subscribe(details: SubscriptionDetails) {
        details.topics.forEach {
            subscriptions[it] = details
        }
    }

    override fun unsubscribe() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateDeliveryInterval(interval: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
