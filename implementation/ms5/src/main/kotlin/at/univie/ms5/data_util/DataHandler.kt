package at.univie.ms5.data_util

import at.univie.messaging.messages.DataObject
import org.springframework.stereotype.Component

@Component
class DataHandler(
        private val dataFactory: DataFactory,
        private val fileReader: FileReader
) {
    fun getDataObjects(): Collection<DataObject> {
        val dataRows = fileReader.getDataRows()
        val dataObjects = dataRows.map { dataRow -> dataFactory.getDataObject(dataRow) }
        return fogData(dataObjects)
    }

    fun fogData(data: Collection<DataObject>): Collection<DataObject> {
        return data.groupBy { it.date }.map { group ->
            val date = group.key
            val dataObjects = group.value
            val average = dataObjects.sumByDouble { it.getData() } / dataObjects.size
            dataFactory.getDataObject(date, average)
        }
    }

    fun getTopic() = dataFactory.getTopic()
}