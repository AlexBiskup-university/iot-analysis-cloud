package at.univie.ms5.configuration.properties

import at.univie.messagequeue.Message.Priority
import at.univie.messagequeue.Message.Priority.LOW
import at.univie.ms5.configuration.properties.DataProperties.DataType.TEMPERATURE
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@Validated
@Component
@ConfigurationProperties(prefix = "data", ignoreInvalidFields = false, ignoreUnknownFields = false)
class DataProperties {

    var type: DataType = TEMPERATURE
    @NotBlank
    lateinit var filePath: String
    var messagePriority: Priority = LOW
    var publishingInterval: Long = 0

    enum class DataType {
        TEMPERATURE, LIGHT_INTENSITY
    }

}
