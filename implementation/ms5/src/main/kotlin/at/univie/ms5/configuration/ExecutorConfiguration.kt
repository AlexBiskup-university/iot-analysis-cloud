package at.univie.ms5.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.Executor
import java.util.concurrent.Executors.newCachedThreadPool

@Configuration
class ExecutorConfiguration {

    @Bean
    fun executor(): Executor = newCachedThreadPool()

}
