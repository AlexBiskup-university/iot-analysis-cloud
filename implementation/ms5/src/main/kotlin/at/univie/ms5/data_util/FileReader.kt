package at.univie.ms5.data_util

import at.univie.ms5.configuration.properties.DataProperties
import org.slf4j.Logger
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Component
import java.io.BufferedReader
import java.io.File
import java.time.LocalDate
import java.time.LocalDate.parse
import java.time.format.DateTimeFormatter.ofPattern

@Component
class FileReader(
    dataProperties: DataProperties,
    private val resourceLoader: ResourceLoader,
    private val log: Logger
) {

    private val filePath = dataProperties.filePath

    fun getDataRows(): Collection<DataRow> {
        val inputStream = try {
            if (filePath.startsWith("classpath:")) resourceLoader.getResource(filePath).inputStream else File(filePath).inputStream()
        } catch (exception: Exception) {
            log.warn("File at path=$filePath could not be opened: ${exception.message}")
            return emptyList()
        }
        return inputStream.bufferedReader().mapLines {
            try {
                val values = it.split(";")
                val row = values[0].toInt()
                val sensor = values[2]
                val date = parse(values[3], ofPattern("dd-MM-yy HH:mm:ss"))
                val temperature = values[4].run { if (this.isBlank()) 0.0 else this.toDouble() }
                val lightIntensity = values[5].run { if (this.isBlank()) 0.0 else this.toDouble() }
                DataRow(row, sensor, date, temperature, lightIntensity)
            } catch (e: Exception) {
                log.error("Couldn't parse invalid line: $it.")
                null
            }
        }.filterNotNull()
    }

    data class DataRow(
        val row: Int,
        val sensor: String,
        val date: LocalDate,
        val temperature: Double,
        val lightIntensity: Double
    )

}

fun <T> BufferedReader.mapLines(block: (line: String) -> T): List<T> {
    val results = mutableListOf<T>()
    this.forEachLine {
        val result = block(it)
        results.add(result)
    }
    return results
}
