package at.univie.ms1client.messagequeue.remote.monitor

import at.univie.messagequeue.monitor.MonitoringSession
import org.springframework.web.socket.WebSocketSession
import java.util.*

class RemoteMonitoringSession(
    private val requester: MonitorRequester,
    private val session: WebSocketSession,
    private val monitorManager: MonitorManager
): MonitoringSession {

    init {
        requester.connect()
    }

    override fun close() {
        if(!isOpen()) return
        requester.disconnect()
        monitorManager.removeMonitor()
        session.close()
    }

    override fun terminate(clientId: UUID) {
        requireOpenSession()
        requester.terminate(clientId)
    }

    override fun isOpen(): Boolean = session.isOpen

    private fun requireOpenSession() {
        require(isOpen()) { IllegalStateException("Session is already closed.") }
    }
}
