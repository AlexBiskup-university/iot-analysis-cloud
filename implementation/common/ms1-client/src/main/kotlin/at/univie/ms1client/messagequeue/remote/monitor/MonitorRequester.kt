package at.univie.ms1client.messagequeue.remote.monitor

import at.univie.messagequeue.monitor.Monitor
import at.univie.messaging.ms1.requests.monitor.ConnectRequest
import at.univie.messaging.ms1.requests.monitor.DisconnectRequest
import at.univie.messaging.ms1.requests.monitor.MonitorRequest
import at.univie.messaging.ms1.requests.monitor.TerminateRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import java.time.LocalDateTime.now
import java.util.*

class MonitorRequester(
    private val session: WebSocketSession,
    private val monitor: Monitor,
    private val objectMapper: ObjectMapper
) {

    fun connect() {
        sendRequest(ConnectRequest(now(), monitor.id, monitor.name))
    }

    fun disconnect() {
        sendRequest(DisconnectRequest(now(), monitor.id))
    }

    fun terminate(clientId: UUID) {
        sendRequest(TerminateRequest(now(), monitor.id, clientId))
    }

    private fun sendRequest(request: MonitorRequest) {
        val message = TextMessage(objectMapper.writeValueAsString(request))
        session.sendMessage(message)
    }

}
