package at.univie.ms1client.messagequeue.remote

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import at.univie.messaging.ms1.requests.client.ClientRequestMarshaller
import at.univie.ms1client.messagequeue.remote.client.ClientManager
import at.univie.ms1client.messagequeue.remote.client.ClientRequester
import at.univie.ms1client.messagequeue.remote.client.RemoteClientSession
import at.univie.ms1client.messagequeue.remote.client.ServerRequestHandler
import at.univie.ms1client.messagequeue.remote.configuration.MessageQueueProperties
import at.univie.ms1client.messagequeue.remote.monitor.MonitorManager
import at.univie.ms1client.messagequeue.remote.monitor.MonitorRequester
import at.univie.ms1client.messagequeue.remote.monitor.MonitorServerRequestHandler
import at.univie.ms1client.messagequeue.remote.monitor.RemoteMonitoringSession
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.socket.client.WebSocketClient

class RemoteMessageQueue(
    private val webSocketClient: WebSocketClient,
    private val clientManager: ClientManager,
    private val monitorManager: MonitorManager,
    private val clientRequestMarshaller: ClientRequestMarshaller,
    private val objectMapper: ObjectMapper,
    private val messageQueueProperties: MessageQueueProperties
) : MessageQueue {

    override fun connect(client: Client): ClientSession {
        require(!clientManager.isClientPresent()) { "A client is already connected." }
        val serverRequestHandler = ServerRequestHandler(objectMapper, client)
        val session = webSocketClient.doHandshake(serverRequestHandler, "${messageQueueProperties.url}/clients").get()
        val requester = ClientRequester(session, client, clientRequestMarshaller)
        return RemoteClientSession(session, clientManager, requester, serverRequestHandler)
    }

    override fun connect(monitor: Monitor): MonitoringSession {
        require(!monitorManager.isMonitorPresent()) { "A monitor is already connected." }
        val requestHandler = MonitorServerRequestHandler(monitor.eventHandler, objectMapper)
        val session = webSocketClient.doHandshake(requestHandler, "${messageQueueProperties.url}/monitors").get()
        val requester = MonitorRequester(session, monitor, objectMapper)
        return RemoteMonitoringSession(requester, session, monitorManager)
    }

}
