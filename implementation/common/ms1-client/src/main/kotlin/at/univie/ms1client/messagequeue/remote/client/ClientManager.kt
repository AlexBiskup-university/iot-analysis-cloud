package at.univie.ms1client.messagequeue.remote.client

import at.univie.messagequeue.client.Client
import org.springframework.stereotype.Component

@Component
class ClientManager {

    private var client: Client? = null

    fun setClient(client: Client) {
        this.client = client
    }

    fun removeClient() {
        client = null
    }

    fun getClient(): Client = client ?: throw IllegalStateException("No client is present.")

    fun isClientPresent(): Boolean = client != null

}
