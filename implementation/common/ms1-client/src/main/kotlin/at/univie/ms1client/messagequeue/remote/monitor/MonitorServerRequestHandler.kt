package at.univie.ms1client.messagequeue.remote.monitor

import at.univie.messagequeue.monitor.EventHandler
import at.univie.messaging.ms1.requests.monitor.MonitorServerRequest
import at.univie.messaging.ms1.requests.monitor.ReceiveErrorRequest
import at.univie.messaging.ms1.requests.monitor.ReceiveEventRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler

class MonitorServerRequestHandler(
    private val eventHandler: EventHandler,
    private val objectMapper: ObjectMapper
) : TextWebSocketHandler() {

    override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        when (val request = objectMapper.readValue(message.payload, MonitorServerRequest::class.java)) {
            is ReceiveEventRequest -> eventHandler.handleEvent(request.event)
            is ReceiveErrorRequest -> throw RuntimeException(request.error)
        }
    }

}
