package at.univie.ms1client.messagequeue.remote.monitor

import at.univie.messagequeue.monitor.EventHandler
import at.univie.messagequeue.monitor.Monitor
import java.util.*

data class SimpleMonitor(
        override val name: String,
        override val eventHandler: EventHandler
): Monitor {
    override val id: UUID = UUID.randomUUID()
}