package at.univie.ms1client.messagequeue.remote.client

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.SubscriptionDetails
import org.springframework.web.socket.WebSocketSession

class RemoteClientSession(
    private val session: WebSocketSession,
    private val clientManager: ClientManager,
    private val requester: ClientRequester,
    private val requestHandler: ServerRequestHandler
) : ClientSession {

    init {
        requester.connect()
    }

    override fun isOpen() = session.isOpen

    private val subscribed: Boolean
        get() = requestHandler.isMessageHandlerPresent()

    override fun close() {
        if(!isOpen()) return
        if (subscribed) unsubscribe()
        clientManager.removeClient()
        requester.disconnect()
        session.close()
    }

    override fun publish(message: Message) {
        requireOpenSession()
        requester.publish(message)
    }

    override fun subscribe(details: SubscriptionDetails) {
        requireOpenSession()
        require(!subscribed) { "Session is already subscribed. Unsubscribe before subscribing again." }
        requestHandler.setMessageHandler(details.messageHandler)
        requester.subscribe(details.topics, details.deliveryInterval)
    }

    override fun unsubscribe() {
        requireOpenSession()
        requestHandler.removeMessageHandler()
        requester.unsubscribe()
    }

    override fun updateDeliveryInterval(interval: Long) {
        requireOpenSession()
        requester.updateSubscriptionDetails(interval)
    }

    private fun requireOpenSession() {
        require(isOpen()) { IllegalStateException("Session is already closed.") }
    }

}
