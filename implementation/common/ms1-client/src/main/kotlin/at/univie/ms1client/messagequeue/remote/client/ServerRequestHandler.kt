package at.univie.ms1client.messagequeue.remote.client

import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.MessageHandler
import at.univie.messaging.ms1.requests.client.ReceiveErrorRequest
import at.univie.messaging.ms1.requests.client.ReceiveMessageRequest
import at.univie.messaging.ms1.requests.client.ServerRequest
import at.univie.messaging.ms1.requests.client.TerminateRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler

class ServerRequestHandler(
    private val objectMapper: ObjectMapper,
    private val client: Client
) : TextWebSocketHandler() {

    private var messageHandler: MessageHandler? = null

    fun isMessageHandlerPresent(): Boolean = messageHandler != null

    fun removeMessageHandler() {
        messageHandler = null
    }


    fun setMessageHandler(messageHandler: MessageHandler) {
        this.messageHandler = messageHandler
    }

    override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        when (val response = objectMapper.readValue(message.payload, ServerRequest::class.java)) {
            is ReceiveMessageRequest -> messageHandler?.handleMessage(response.message)
            is TerminateRequest -> client.terminationHandler.handleTermination()
            is ReceiveErrorRequest -> throw RuntimeException(response.error)
        }
    }

}
