package at.univie.ms1client.messagequeue.remote.configuration

import at.univie.messagequeue.MessageQueue
import at.univie.messaging.ms1.requests.client.ClientRequestMarshaller
import at.univie.ms1client.messagequeue.remote.RemoteMessageQueue
import at.univie.ms1client.messagequeue.remote.client.ClientManager
import at.univie.ms1client.messagequeue.remote.monitor.MonitorManager
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.client.WebSocketClient

@Configuration
class MessageQueueConfiguration {

    @Bean("messageQueue")
    @ConditionalOnProperty(name = ["message-queue.enabled"], havingValue = "true", matchIfMissing = false)
    fun remoteMessageQueue(
        webSocketClient: WebSocketClient,
        clientManager: ClientManager,
        monitorManager: MonitorManager,
        clientRequestMarshaller: ClientRequestMarshaller,
        objectMapper: ObjectMapper,
        messageQueueProperties: MessageQueueProperties
    ): MessageQueue =
        RemoteMessageQueue(webSocketClient, clientManager, monitorManager, clientRequestMarshaller, objectMapper, messageQueueProperties)

}
