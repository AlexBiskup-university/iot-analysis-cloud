package at.univie.ms1client.messagequeue.remote.monitor

import at.univie.messagequeue.monitor.Monitor
import org.springframework.stereotype.Component

@Component
class MonitorManager {

    private var monitor: Monitor? = null

    fun setMonitor(monitor: Monitor) {
        this.monitor = monitor
    }

    fun removeMonitor() {
        monitor = null
    }

    fun getMonitor(): Monitor = monitor ?: throw IllegalStateException("No monitor is present.")

    fun isMonitorPresent(): Boolean = monitor != null

}
