package at.univie.ms1client.messagequeue.remote.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@Validated
@ConfigurationProperties(prefix = "message-queue", ignoreInvalidFields = false, ignoreUnknownFields = false)
@ConstructorBinding
class MessageQueueProperties(
    @NotBlank
    val url: String,
    val enabled: Boolean = true,
    val clientName: String = "NO-NAME",
    val deliveryInterval: Long = 0
)

@Configuration
@EnableConfigurationProperties(MessageQueueProperties::class)
class ConfigureProperties
