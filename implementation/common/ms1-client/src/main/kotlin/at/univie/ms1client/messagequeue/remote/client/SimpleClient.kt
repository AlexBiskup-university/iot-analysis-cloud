package at.univie.ms1client.messagequeue.remote.client

import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.TerminationHandler
import java.util.*

data class SimpleClient(
        override val name: String,
        override val terminationHandler: TerminationHandler
) : Client {
    override val id: UUID = UUID.randomUUID()
}