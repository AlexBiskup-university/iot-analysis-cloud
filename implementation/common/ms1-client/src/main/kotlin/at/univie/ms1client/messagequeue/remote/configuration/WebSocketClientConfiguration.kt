package at.univie.ms1client.messagequeue.remote.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.util.concurrent.ListenableFuture
import org.springframework.util.concurrent.ListenableFutureTask
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.client.WebSocketClient
import org.springframework.web.socket.client.standard.StandardWebSocketClient
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator.OverflowStrategy
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator.OverflowStrategy.DROP
import java.util.concurrent.Callable

@Configuration
class WebSocketClientConfiguration {

    @Bean
    fun webSocketClient(): WebSocketClient = ConcurrentWebSocketClient(2000, 4096, DROP)

}

class ConcurrentWebSocketClient(
    private val sendTimeLimit: Int,
    private val bufferSizeLimit: Int,
    private val overflowStrategy: OverflowStrategy
) : StandardWebSocketClient() {

    override fun doHandshake(webSocketHandler: WebSocketHandler, uriTemplate: String, vararg uriVariables: Any?): ListenableFuture<WebSocketSession> {
        val task: Callable<WebSocketSession> = Callable {
            val session = super.doHandshake(webSocketHandler, uriTemplate, *uriVariables)
            ConcurrentWebSocketSessionDecorator(session.get(), sendTimeLimit, bufferSizeLimit, overflowStrategy)
        }
        return ListenableFutureTask(task).also { it.run() }
    }

}

