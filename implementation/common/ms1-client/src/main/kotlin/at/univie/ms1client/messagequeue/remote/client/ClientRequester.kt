package at.univie.ms1client.messagequeue.remote.client

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.Client
import at.univie.messaging.ms1.requests.client.*
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import java.time.LocalDateTime.now

class ClientRequester(
    private val session: WebSocketSession,
    private val client: Client,
    private val marshaller: ClientRequestMarshaller
) {

    fun connect() {
        sendRequest(ConnectRequest(client.id, now(), client.name))
    }

    fun disconnect() {
        sendRequest(DisconnectRequest(client.id, now()))
    }

    fun publish(message: Message) {
        sendRequest(PublishRequest(client.id, now(), message))
    }

    fun subscribe(topics: List<String>, deliveryInterval: Long) {
        sendRequest(SubscribeRequest(client.id, now(), topics, deliveryInterval))
    }

    fun unsubscribe() {
        sendRequest(UnsubscribeRequest(client.id, now()))
    }

    fun updateSubscriptionDetails(deliveryInterval: Long) {
        sendRequest(UpdateSubscriptionRequest(client.id, now(), deliveryInterval))
    }

    private fun sendRequest(request: ClientRequest) {
        val message = TextMessage(marshaller.marshall(request))
        session.sendMessage(message)
    }
}
