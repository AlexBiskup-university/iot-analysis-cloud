package at.univie.messaging.ms1.requests.monitor

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component

@Component
class MonitorServerRequestMarshaller(
        private val objectMapper: ObjectMapper
) {

    fun marshall(request: MonitorServerRequest): String {
        return objectMapper.writeValueAsString(request)
    }

    fun unmarshall(request: String): MonitorServerRequest {
        return objectMapper.readValue(request, MonitorServerRequest::class.java)
    }

}