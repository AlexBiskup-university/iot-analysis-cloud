package at.univie.messaging.messages

object MessageTopics {
    private const val TOPIC_PREFIX = "/topic"
    const val SENSOR_DATA_TEMPERATURE = "$TOPIC_PREFIX/sensor-data/temperature"
    const val SENSOR_DATA_LIGHT_INTENSITY = "$TOPIC_PREFIX/sensor-data/light-intensity"
    const val ANALYZED_TEMPERATURE_DATA = "$SENSOR_DATA_TEMPERATURE/analyzed"
    const val ANALYZED_LIGHT_INTENSITY_DATA = "$SENSOR_DATA_LIGHT_INTENSITY/analyzed"
    const val TEMPERATURE_DATA_WARNING = "$SENSOR_DATA_TEMPERATURE/warning"
    const val LIGHT_INTENSITY_DATA_WARNING = "$SENSOR_DATA_LIGHT_INTENSITY/warning"
}

object MessageTopicCollections {
    val MS4_TOPICS = listOf(
        MessageTopics.SENSOR_DATA_LIGHT_INTENSITY,
        MessageTopics.ANALYZED_LIGHT_INTENSITY_DATA,
        MessageTopics.SENSOR_DATA_TEMPERATURE,
        MessageTopics.ANALYZED_TEMPERATURE_DATA,
        MessageTopics.TEMPERATURE_DATA_WARNING,
        MessageTopics.LIGHT_INTENSITY_DATA_WARNING
    )

    val MS5_TOPICS = listOf(MessageTopics.SENSOR_DATA_LIGHT_INTENSITY, MessageTopics.SENSOR_DATA_TEMPERATURE)
}
