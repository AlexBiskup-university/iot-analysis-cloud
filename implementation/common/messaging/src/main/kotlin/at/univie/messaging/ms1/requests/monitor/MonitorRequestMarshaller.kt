package at.univie.messaging.ms1.requests.monitor

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component

@Component
class MonitorRequestMarshaller (
private val objectMapper: ObjectMapper
) {

    fun marshall(request: MonitorRequest): String {
        return objectMapper.writeValueAsString(request)
    }

    fun unmarshall(request: String): MonitorRequest {
        return objectMapper.readValue(request, MonitorRequest::class.java)
    }

}