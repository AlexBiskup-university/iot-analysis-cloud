package at.univie.messaging

import at.univie.messagequeue.client.TerminationHandler
import org.slf4j.Logger
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.stereotype.Component
import kotlin.system.exitProcess

@Component
class SpringBootTerminationHandler(
    private val applicationContext: ConfigurableApplicationContext,
    private val log: Logger
) : TerminationHandler {

    override fun handleTermination() {
        log.info("Shutdown application")
        applicationContext.close()
        exitProcess(0)
    }
}
