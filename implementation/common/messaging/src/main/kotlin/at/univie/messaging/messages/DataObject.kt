package at.univie.messaging.messages

import java.time.LocalDate
import java.util.*

abstract class DataObject(
        val date: LocalDate
) {
    val id: UUID = UUID.randomUUID()

    abstract fun getData(): Double
}