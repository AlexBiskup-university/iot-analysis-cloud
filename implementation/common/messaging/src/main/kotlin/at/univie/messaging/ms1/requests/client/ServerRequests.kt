package at.univie.messaging.ms1.requests.client

import at.univie.messagequeue.Message
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.time.LocalDateTime

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "method"
)
@JsonSubTypes(
    JsonSubTypes.Type(name = "RECEIVE_MESSAGE", value = ReceiveMessageRequest::class),
    JsonSubTypes.Type(name = "RECEIVE_ERROR", value = ReceiveErrorRequest::class),
    JsonSubTypes.Type(name = "TERMINATE", value = TerminateRequest::class)
)
sealed class ServerRequest(
    val timestamp: LocalDateTime
)

class ReceiveMessageRequest(
    timestamp: LocalDateTime,
    val message: Message
) : ServerRequest(timestamp)

class ReceiveErrorRequest(
    timestamp: LocalDateTime,
    val error: String
) : ServerRequest(timestamp)

class TerminateRequest(
    timestamp: LocalDateTime
) : ServerRequest(timestamp)
