package at.univie.messaging.ms1.requests.client

import com.fasterxml.jackson.databind.ObjectMapper

class ServerRequestMarshaller(
        private val objectMapper: ObjectMapper
) {

    fun marshall(request: ServerRequest): String {
        return objectMapper.writeValueAsString(request)
    }

    fun unmarshall(request: String): ServerRequest {
        return objectMapper.readValue(request, ServerRequest::class.java)
    }

}