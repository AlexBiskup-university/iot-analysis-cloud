package at.univie.messaging.messages

import com.fasterxml.jackson.annotation.JsonGetter
import java.time.LocalDate

class LightIntensityData(
        date: LocalDate,
        private val lightIntensity: Double
) : DataObject(date) {

    @JsonGetter("lightIntensity")
    override fun getData(): Double {
        return lightIntensity
    }
}
