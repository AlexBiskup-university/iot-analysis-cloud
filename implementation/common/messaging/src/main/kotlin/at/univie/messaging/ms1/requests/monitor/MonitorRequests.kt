package at.univie.messaging.ms1.requests.monitor

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.time.LocalDateTime
import java.util.*

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "method"
)
@JsonSubTypes(
        JsonSubTypes.Type(name = "CONNECT", value = ReceiveEventRequest::class),
        JsonSubTypes.Type(name = "DISCONNECT", value = ReceiveErrorRequest::class),
        JsonSubTypes.Type(name = "TERMINATE", value = ReceiveErrorRequest::class)
)
sealed class MonitorRequest(
        val timestamp: LocalDateTime,
        val monitorId: UUID
)

class ConnectRequest(
        timestamp: LocalDateTime,
        monitorId: UUID,
        val monitorName: String
): MonitorRequest(timestamp, monitorId)

class DisconnectRequest(
        timestamp: LocalDateTime,
        monitorId: UUID
): MonitorRequest(timestamp, monitorId)

class TerminateRequest(
        timestamp: LocalDateTime,
        monitorId: UUID,
        val clientId: UUID
): MonitorRequest(timestamp, monitorId)
