package at.univie.messaging.ms1.requests.monitor

import at.univie.messagequeue.monitor.events.Event
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.time.LocalDateTime

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "method"
)
@JsonSubTypes(
        JsonSubTypes.Type(name = "RECEIVE_EVENT", value = ReceiveEventRequest::class),
        JsonSubTypes.Type(name = "RECEIVE_ERROR", value = ReceiveErrorRequest::class)
)
sealed class MonitorServerRequest(
        val timestamp: LocalDateTime
)

class ReceiveEventRequest(
        timestamp: LocalDateTime,
        val event: Event
): MonitorServerRequest(timestamp)

class ReceiveErrorRequest(
        timestamp: LocalDateTime,
        val error: String
): MonitorServerRequest(timestamp)

