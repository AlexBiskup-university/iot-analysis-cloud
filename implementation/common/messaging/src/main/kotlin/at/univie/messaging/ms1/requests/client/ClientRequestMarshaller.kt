package at.univie.messaging.ms1.requests.client

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component

@Component
class ClientRequestMarshaller(
private val objectMapper: ObjectMapper
) {

    fun marshall(request: ClientRequest): String {
        return objectMapper.writeValueAsString(request)
    }

    fun unmarshall(request: String): ClientRequest {
        return objectMapper.readValue(request, ClientRequest::class.java)
    }

}