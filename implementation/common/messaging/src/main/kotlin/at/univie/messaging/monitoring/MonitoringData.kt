package at.univie.messaging.monitoring

import java.time.LocalDateTime
import java.util.*

data class MonitoringData(
        val exchangedMessages: Int,
        val errors: Int,
        val connectedClients: List<ClientDetails>
)

data class ClientDetails (
        val name: String,
        val id: UUID,
        val connectionStart: LocalDateTime
)

