package at.univie.messaging.ms1.requests.client

import at.univie.messagequeue.Message
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME
import java.time.LocalDateTime
import java.util.*

@JsonTypeInfo(
        use = NAME,
        include = PROPERTY,
        property = "method"
)
@JsonSubTypes(
        Type(name = "CONNECT", value = ConnectRequest::class),
        Type(name = "DISCONNECT", value = DisconnectRequest::class),
        Type(name = "PUBLISH", value = PublishRequest::class),
        Type(name = "SUBSCRIBE", value = SubscribeRequest::class),
        Type(name = "UNSUBSCRIBE", value = UnsubscribeRequest::class),
        Type(name = "UPDATE_SUBSCRIPTION", value = UpdateSubscriptionRequest::class)
)
sealed class ClientRequest(
        val clientId: UUID,
        val timestamp: LocalDateTime
)

class ConnectRequest(
        clientId: UUID,
        timestamp: LocalDateTime,
        val clientName: String
): ClientRequest(clientId, timestamp)

class DisconnectRequest(
        clientId: UUID,
        timestamp: LocalDateTime
): ClientRequest(clientId, timestamp)

class PublishRequest(
        clientId: UUID,
        timestamp: LocalDateTime,
        val message: Message
): ClientRequest(clientId, timestamp)

class SubscribeRequest(
        clientId: UUID,
        timestamp: LocalDateTime,
        val topics: List<String>,
        val deliveryInterval: Long
): ClientRequest(clientId, timestamp)

class UnsubscribeRequest(
        clientId: UUID,
        timestamp: LocalDateTime
): ClientRequest(clientId, timestamp)

class UpdateSubscriptionRequest(
        clientId: UUID,
        timestamp: LocalDateTime,
        val deliveryInterval: Long
): ClientRequest(clientId, timestamp)