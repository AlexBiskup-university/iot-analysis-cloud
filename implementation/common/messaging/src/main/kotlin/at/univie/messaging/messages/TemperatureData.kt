package at.univie.messaging.messages

import com.fasterxml.jackson.annotation.JsonGetter
import java.time.LocalDate

class TemperatureData(
        date: LocalDate,
        private val temperature: Double
) : DataObject(date) {

    @JsonGetter("temperature")
    override fun getData(): Double {
        return temperature
    }
}
