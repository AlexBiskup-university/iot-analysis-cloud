package at.univie.messagequeue.monitor

import java.util.*

interface MonitoringSession {

    fun close()

    fun terminate(clientId: UUID)

    fun isOpen(): Boolean

}
