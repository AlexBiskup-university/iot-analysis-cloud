package at.univie.messagequeue.client

import at.univie.messagequeue.Message

interface MessageHandler {

    fun handleMessage(message: Message)

}