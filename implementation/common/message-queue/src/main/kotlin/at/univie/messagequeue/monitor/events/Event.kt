package at.univie.messagequeue.monitor.events

import at.univie.messagequeue.Message.Priority
import at.univie.messagequeue.monitor.events.SubscribeEvent.SubscriptionDetails
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.time.LocalDateTime
import java.util.*

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type"
)
@JsonSubTypes(
    Type(name = "CONNECT", value = ConnectEvent::class),
    Type(name = "DISCONNECT", value = DisconnectEvent::class),
    Type(name = "PUBLISH", value = PublishEvent::class),
    Type(name = "SUBSCRIBE", value = SubscribeEvent::class),
    Type(name = "UNSUBSCRIBE", value = UnsubscribeEvent::class),
    Type(name = "UPDATE_SUBSCRIPTION", value = UpdateSubscriptionEvent::class),
    Type(name = "ERROR", value = ErrorEvent::class)
)
sealed class Event(
    val clientId: UUID,
    val timestamp: LocalDateTime
)

class ConnectEvent(
    clientId: UUID,
    timestamp: LocalDateTime,
    val clientName: String
) : Event(clientId, timestamp)

class DisconnectEvent(
    clientId: UUID,
    timestamp: LocalDateTime
) : Event(clientId, timestamp)

class PublishEvent(
    clientId: UUID,
    timestamp: LocalDateTime,
    val details: MessageDetails
) : Event(clientId, timestamp) {
    data class MessageDetails(val topic: String, val priority: Priority)
}

class SubscribeEvent(
    clientId: UUID,
    timestamp: LocalDateTime,
    val details: SubscriptionDetails
) : Event(clientId, timestamp) {
    data class SubscriptionDetails(val topics: List<String>, val deliveryInterval: Long)
}

class UnsubscribeEvent(
    clientId: UUID,
    timestamp: LocalDateTime
) : Event(clientId, timestamp)

class UpdateSubscriptionEvent(
    clientId: UUID,
    timestamp: LocalDateTime,
    val updatedDeliveryInterval: Long
) : Event(clientId, timestamp)

class ErrorEvent(
    clientId: UUID,
    timestamp: LocalDateTime,
    val message: String
) : Event(clientId, timestamp)

