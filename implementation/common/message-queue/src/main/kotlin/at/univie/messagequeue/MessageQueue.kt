package at.univie.messagequeue

import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession

interface MessageQueue {

    fun connect(client: Client): ClientSession

    fun connect(monitor: Monitor): MonitoringSession

}
