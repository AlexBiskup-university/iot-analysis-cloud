package at.univie.messagequeue.client

interface TerminationHandler {

    fun handleTermination()

}