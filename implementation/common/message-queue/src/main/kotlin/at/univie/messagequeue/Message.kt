package at.univie.messagequeue

import java.time.LocalDateTime

data class Message(
        val topic: String,
        val timestamp: LocalDateTime,
        val priority: Priority,
        val payload: String
) {
    enum class Priority{
        LOW, MEDIUM, HIGH, CRITICAL
    }
}

object MessageComparators {
    fun getDefault() = nullsLast(compareByDescending<Message> { it.priority.ordinal }.thenBy { it.timestamp })
}
