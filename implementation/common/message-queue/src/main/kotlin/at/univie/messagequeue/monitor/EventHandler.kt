package at.univie.messagequeue.monitor

import at.univie.messagequeue.monitor.events.Event

interface EventHandler {

    fun handleEvent(event: Event)

}