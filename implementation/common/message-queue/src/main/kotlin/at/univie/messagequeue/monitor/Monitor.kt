package at.univie.messagequeue.monitor

import java.util.*

interface Monitor {

    val id: UUID
    val name: String
    val eventHandler: EventHandler

}