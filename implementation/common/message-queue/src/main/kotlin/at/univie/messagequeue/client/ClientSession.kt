package at.univie.messagequeue.client

import at.univie.messagequeue.Message

interface ClientSession {

    fun isOpen(): Boolean

    fun close()

    fun publish(message: Message)

    fun subscribe(details: SubscriptionDetails)

    fun unsubscribe()

    fun updateDeliveryInterval(interval: Long)

}