package at.univie.messagequeue.client

import java.util.*

interface Client {

    val id: UUID
    val name: String
    val terminationHandler: TerminationHandler

}