package at.univie.messagequeue.client

data class SubscriptionDetails(
        val topics: List<String>,
        val messageHandler: MessageHandler,
        val deliveryInterval: Long = 0
)
