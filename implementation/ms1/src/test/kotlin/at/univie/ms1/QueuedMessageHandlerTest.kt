package at.univie.ms1

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.MessageHandler
import at.univie.ms1.messagequeue.QueuedMessageHandler
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertTimeoutPreemptively
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.time.temporal.ChronoUnit
import java.util.concurrent.Executors.newScheduledThreadPool
import java.util.concurrent.ScheduledExecutorService

class QueuedMessageHandlerTest {


    private val executor: ScheduledExecutorService = newScheduledThreadPool(0)

    /*
    * Test that the queued message handler sends messages according to the interval
    * */
    @Test
    fun intervalTest() {
        val interval: Long = 2 * 1000
        val sendTimestamps: MutableList<LocalDateTime> = mutableListOf()
        val numberOfTestMessages = 2
        var numberOfMessagesHandled = 0
        var receiveOK = false

        val initMessage = Message("/init", now(), Message.Priority.MEDIUM, "Hello World!")
        val testMessage = Message("/test", now(), Message.Priority.MEDIUM, "Hello World!")

        val handler = object : MessageHandler {
            override fun handleMessage(message: Message) {
                if (message.topic == testMessage.topic) {
                    val receiveTimeStamp = now()
                    if ((receiveOK || numberOfMessagesHandled == 0)
                            && receiveTimeStamp.isAfter(sendTimestamps[numberOfMessagesHandled].truncatedTo(ChronoUnit.SECONDS).plus(interval*(numberOfMessagesHandled + 1), ChronoUnit.MILLIS))) {
                        receiveOK = true
                    }
                    ++numberOfMessagesHandled
                }
            }
        }
        val queuedMessageHandler = QueuedMessageHandler(interval, handler, executor) {}

        queuedMessageHandler.handleMessage(initMessage)

        for (i in 1..numberOfTestMessages) {
            sendTimestamps.add(now())
            queuedMessageHandler.handleMessage(testMessage)
        }

        assertTimeoutPreemptively(Duration.ofMillis(interval * (numberOfTestMessages + 1)), message = "Timed out.") {
            while (numberOfTestMessages != numberOfMessagesHandled) Thread.sleep(5)
            assert(receiveOK)
        }
    }
}
