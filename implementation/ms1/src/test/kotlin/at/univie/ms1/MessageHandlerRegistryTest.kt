package at.univie.ms1

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.MessageHandler
import at.univie.messaging.messages.MessageTopics
import at.univie.ms1.messagequeue.MessageHandlerRegistry
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class MessageHandlerRegistryTest {

    private val messageHandlerRegistry = MessageHandlerRegistry()

    @Test
    fun registerAndDeregisterTest() {
        val topicA = "/topicA"
        val topicB = "/topicB"
        val handlerA = Mockito.mock(MessageHandler::class.java)
        val handlerB = Mockito.mock(MessageHandler::class.java)
        val handlerAB = Mockito.mock(MessageHandler::class.java)

        messageHandlerRegistry.register(topicA, handlerA)
        messageHandlerRegistry.register(topicB, handlerB)
        messageHandlerRegistry.register(listOf(topicA, topicB), handlerAB)

        Assertions.assertEquals(messageHandlerRegistry.getMessageHandlersForTopic(topicA), listOf(handlerA, handlerAB))
        Assertions.assertEquals(messageHandlerRegistry.getMessageHandlersForTopic(topicB), listOf(handlerB, handlerAB))

        messageHandlerRegistry.deregister(handlerA)
        messageHandlerRegistry.deregister(handlerB)

        Assertions.assertEquals(messageHandlerRegistry.getMessageHandlersForTopic(topicA), listOf(handlerAB))
        Assertions.assertEquals(messageHandlerRegistry.getMessageHandlersForTopic(topicB), listOf(handlerAB))

        messageHandlerRegistry.deregister(handlerAB)

        Assertions.assertEquals(messageHandlerRegistry.getMessageHandlersForTopic(topicA), emptyList<MessageHandler>())
        Assertions.assertEquals(messageHandlerRegistry.getMessageHandlersForTopic(topicB), emptyList<MessageHandler>())
    }
}