package at.univie.ms1

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.MessageHandler
import at.univie.messaging.messages.MessageTopics
import at.univie.ms1.messagequeue.MessageBroker
import at.univie.ms1.messagequeue.MessageHandlerRegistry
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertTimeoutPreemptively
import org.springframework.beans.factory.annotation.Autowired
import java.time.Duration
import java.time.LocalDateTime.now
import java.time.temporal.ChronoUnit

class MessageBrokerTest {

    private val messageHandlerRegistry = MessageHandlerRegistry()
    private val messageBroker = MessageBroker(messageHandlerRegistry)

    /*
    * Test that the message broker sends messages to correct messageHandler
    * */
    @Test
    fun sendTest() {

        var lightHandlerReceived : Boolean = false
        var lightHandlerOK : Boolean = false
        var temperatureHandlerReceived : Boolean = false
        var temperatureHandlerOK : Boolean = false

        val lightHandler = object : MessageHandler {
            override fun handleMessage(message: Message) {
                lightHandlerReceived = true
                lightHandlerOK = message.topic == MessageTopics.SENSOR_DATA_LIGHT_INTENSITY
            }
        }
        val temperatureHandler = object : MessageHandler {
            override fun handleMessage(message: Message) {
                temperatureHandlerReceived = true
                temperatureHandlerOK = message.topic == MessageTopics.SENSOR_DATA_TEMPERATURE
            }
        }

        messageBroker.subscribe(listOf(MessageTopics.SENSOR_DATA_LIGHT_INTENSITY), lightHandler)
        messageBroker.subscribe(listOf(MessageTopics.SENSOR_DATA_TEMPERATURE), temperatureHandler)

        messageBroker.send(Message(MessageTopics.SENSOR_DATA_LIGHT_INTENSITY, now(),Message.Priority.MEDIUM, "light"))
        messageBroker.send(Message(MessageTopics.SENSOR_DATA_TEMPERATURE, now(),Message.Priority.MEDIUM, "temperature"))

        assertTimeoutPreemptively(Duration.ofSeconds(1), message = "Timed out.") {
            while (!lightHandlerReceived && !temperatureHandlerReceived) Thread.sleep(5)
            assert(lightHandlerOK)
            assert(temperatureHandlerOK)
        }
    }
}