package at.univie.ms1

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.MessageHandler
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.ms1.messagequeue.*
import at.univie.ms1.messagequeue.web.client.WebSocketClient
import at.univie.ms1.messagequeue.web.client.WebSocketTerminationHandler
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertTimeoutPreemptively
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.Duration
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

@SpringBootTest
class Ms1ApplicationTests {

    @Autowired
    private lateinit var messageQueue : InMemoryMessageQueue

    @Test
    fun delayedDeliveryTest() {

        val client = WebSocketClient(UUID.randomUUID(), "DDT", Mockito.mock(WebSocketTerminationHandler::class.java))
        val clientSession = messageQueue.connect(client)
        val receivedMessages : MutableList<Message> = mutableListOf()

        val testTopic = "/test"
        val lowPriorityMessage = Message(testTopic, LocalDateTime.now(), Message.Priority.LOW, "Hello World!")
        val mediumPriorityMessage = Message(testTopic, LocalDateTime.now(), Message.Priority.MEDIUM, "Hello World!")
        val highPriorityMessage = Message(testTopic, LocalDateTime.now(), Message.Priority.HIGH, "Hello World!")

        clientSession.publish(lowPriorityMessage)
        clientSession.publish(mediumPriorityMessage)
        clientSession.publish(highPriorityMessage)

        val messageHandler = object : MessageHandler {
            override fun handleMessage(message: Message) {
                receivedMessages.add(message)
            }
        }

        clientSession.subscribe(SubscriptionDetails(listOf(testTopic), messageHandler))

        assertTimeoutPreemptively(Duration.ofMillis(100), message = "Timed out.") {
            while (receivedMessages.size != 3) Thread.sleep(5)
            assertEquals(receivedMessages[0].priority, highPriorityMessage.priority)
            assertEquals(receivedMessages[1].priority, mediumPriorityMessage.priority)
            assertEquals(receivedMessages[2].priority, lowPriorityMessage.priority)
        }
    }
}
