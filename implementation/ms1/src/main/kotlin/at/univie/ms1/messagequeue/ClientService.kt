package at.univie.ms1.messagequeue

import at.univie.messagequeue.client.Client
import org.springframework.stereotype.Component
import java.util.*

@Component
class ClientService {

    private val clients: MutableMap<UUID, Client> = mutableMapOf()
    private val lock = Any()

    fun save(client: Client) {
        synchronized(lock) {
            clients[client.id] = client
        }
    }

    fun get(id: UUID): Client {
        return clients[id] ?: throw IllegalArgumentException("Client with id=$id could not be found.")
    }

    fun remove(client: Client) {
        synchronized(lock) {
            clients.remove(client.id)
        }
    }

    fun isClientPresent(client: Client): Boolean {
        synchronized(lock) {
            return clients.containsKey(client.id)
        }
    }
}
