package at.univie.ms1.messagequeue.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.Executors.newScheduledThreadPool
import java.util.concurrent.ScheduledExecutorService

@Configuration
class ScheduledExecutorConfiguration {

    @Bean
    fun scheduledExecutor(): ScheduledExecutorService = newScheduledThreadPool(0)

}
