package at.univie.ms1.messagequeue

import at.univie.messagequeue.client.MessageHandler
import org.springframework.stereotype.Component

@Component
class MessageHandlerRegistry {
    private val topicToHandlers: MutableMap<String, MutableList<MessageHandler>> = mutableMapOf()
    private val handlerToTopics: MutableMap<MessageHandler, MutableList<String>> = mutableMapOf()

    fun register(topic: String, handler: MessageHandler) {
        topicToHandlers.getOrPut(topic, { mutableListOf() }).add(handler)
        handlerToTopics.getOrPut(handler, { mutableListOf() }).add(topic)
    }

    fun register(topics: List<String>, handler: MessageHandler) {
        topics.forEach { register(it, handler) }
    }

    fun deregister(handler: MessageHandler) {
        val topics = handlerToTopics[handler]
        topics?.forEach {
            topicToHandlers[it]?.remove(handler)
        }
        handlerToTopics.remove(handler)
    }

    fun getMessageHandlersForTopic(topic: String): List<MessageHandler> {
        return topicToHandlers[topic] ?: listOf()
    }
}