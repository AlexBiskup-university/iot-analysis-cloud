package at.univie.ms1.messagequeue.web.client

import at.univie.messagequeue.client.Client
import java.util.*


class WebSocketClient(
    override val id: UUID,
    override val name: String,
    override val terminationHandler: WebSocketTerminationHandler
) : Client
