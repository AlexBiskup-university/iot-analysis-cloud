package at.univie.ms1.messagequeue

import at.univie.messagequeue.monitor.EventHandler
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import at.univie.messagequeue.monitor.events.Event
import org.slf4j.Logger
import java.util.*

class InMemoryMonitoringSession(
    monitor: Monitor,
    private val monitorService: MonitorService,
    private val clientService: ClientService,
    log: Logger
) : MonitoringSession {

    private val monitorProxy = MonitorProxy(monitor, log)

    init {
        monitorService.save(monitorProxy)
    }

    override fun close() {
        monitorService.remove(monitorProxy)
    }

    override fun terminate(clientId: UUID) {
        val client = clientService.get(clientId)
        client.terminationHandler.handleTermination()
    }

    override fun isOpen(): Boolean {
        return monitorService.isMonitorPresent(monitorProxy)
    }

}

class MonitorProxy(
   monitor: Monitor,
   private val log: Logger
) : Monitor {

    override val id = monitor.id
    override val name = monitor.name

    override val eventHandler: EventHandler = object: EventHandler {
        override fun handleEvent(event: Event) {
            try {
                monitor.eventHandler.handleEvent(event)
            } catch (exception: Throwable) {
                log.error("An error occurred while trying to send an event to monitor with id=$id.")
            }
        }
    }
}
