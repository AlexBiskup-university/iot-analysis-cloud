package at.univie.ms1.messagequeue

import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.events.Event
import org.springframework.stereotype.Component
import java.util.*

@Component
class MonitorService: EventEmitter {

    private val monitors: MutableMap<UUID, Monitor> = mutableMapOf()

    fun save(monitor: Monitor) {
        monitors[monitor.id] = monitor
    }

    fun get(id: UUID): Monitor {
        return monitors[id] ?: throw IllegalArgumentException("Monitor with id=$id could not be found.")
    }

    fun remove(monitor: Monitor) {
        monitors.remove(monitor.id)
    }

    fun isMonitorPresent(monitor: Monitor): Boolean {
        return monitors.containsKey(monitor.id)
    }

    override fun emit(event: Event) {
        return monitors.values.forEach{
            it.eventHandler.handleEvent(event)
        }
    }

}

interface EventEmitter {

    fun emit(event: Event)
}
