package at.univie.ms1.messagequeue.web.monitor

import at.univie.messagequeue.monitor.Monitor
import java.util.*

class WebSocketMonitor(
    override val name: String,
    override val id: UUID,
    override val eventHandler: WebSocketEventHandler
) : Monitor {

    fun handleError(error: String) {
        eventHandler.handleError(error)
    }

}
