package at.univie.ms1.messagequeue.web.client

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messaging.ms1.requests.client.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler
import java.util.*

@Component
class ClientRequestHandler(
    private val marshaller: ClientRequestMarshaller,
    val messageQueue: MessageQueue,
    private val log: Logger,
    private val objectMapper: ObjectMapper
) : TextWebSocketHandler() {

    val sessions: MutableMap<UUID, ClientSession> = mutableMapOf()

    override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        try {
            when (val request = marshaller.unmarshall(message.payload)) {
                is ConnectRequest -> connectClient(request, session)
                is DisconnectRequest -> disconnectClient(request)
                is PublishRequest -> handlePublishRequest(request)
                is SubscribeRequest -> handleSubscribeRequest(request, session)
                is UnsubscribeRequest -> handleUnsubscribeRequest(request)
                is UpdateSubscriptionRequest -> handleUpdateSubscriptionRequest(request)
            }
        } catch (exception: Throwable) {
            log.error(exception.message)
            exception.printStackTrace(System.out)
            val requester = ServerRequester(session, ServerRequestMarshaller(objectMapper))
            requester.sendError(exception.message ?: "No message available.")
        }
    }

    private fun connectClient(request: ConnectRequest, session: WebSocketSession) {
        log.info("Connecting: ${request.clientName}")
        val marshaller = ServerRequestMarshaller(objectMapper)
        val requester = ServerRequester(session, marshaller)
        val terminationHandler = WebSocketTerminationHandler(requester)
        val client = WebSocketClient(request.clientId, request.clientName, terminationHandler)
        val clientSession = messageQueue.connect(client)
        sessions[client.id] = clientSession
    }

    private fun disconnectClient(request: DisconnectRequest) {
        log.info("Disconnecting: ${request.clientId}")
        sessions[request.clientId]?.close()
        sessions.remove(request.clientId)
    }

    private fun handlePublishRequest(request: PublishRequest) {
        log.debug("Publishing: ${request.message.topic}")
        sessions[request.clientId]?.publish(request.message)
    }

    private fun handleSubscribeRequest(request: SubscribeRequest, session: WebSocketSession) {
        log.info("Subscribing: ${request.topics}")
        val marshaller = ServerRequestMarshaller(objectMapper)
        val requester = ServerRequester(session, marshaller)
        val webSocketMessageHandler = WebSocketMessageHandler(requester)
        sessions[request.clientId]?.subscribe(SubscriptionDetails(request.topics, webSocketMessageHandler, request.deliveryInterval))
    }

    private fun handleUnsubscribeRequest(request: UnsubscribeRequest) {
        log.info("Unsubscribing: ${request.clientId}")
        sessions[request.clientId]?.unsubscribe()
    }

    private fun handleUpdateSubscriptionRequest(request: UpdateSubscriptionRequest) {
        log.info("Updating: ${request.clientId}")
        sessions[request.clientId]?.updateDeliveryInterval(request.deliveryInterval)
    }

}
