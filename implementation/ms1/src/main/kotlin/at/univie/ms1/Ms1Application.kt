package at.univie.ms1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["at.univie"])
class Ms1Application

fun main(args: Array<String>) {
    runApplication<Ms1Application>(*args)
}
