package at.univie.ms1.messagequeue.web.client

import at.univie.messagequeue.Message
import at.univie.messaging.ms1.requests.client.*
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import java.time.LocalDateTime.now

class ServerRequester(
    private val session: WebSocketSession,
    private val marshaller: ServerRequestMarshaller
) {

    fun sendMessage(message: Message) {
        sendRequest(ReceiveMessageRequest(now(), message))
    }

    fun terminate() {
        sendRequest(TerminateRequest(now()))
    }

    fun sendError(error: String) {
        sendRequest(ReceiveErrorRequest(now(), error))
    }

    private fun sendRequest(request: ServerRequest) {
        val message = TextMessage(marshaller.marshall(request))
        session.sendMessage(message)
    }
}
