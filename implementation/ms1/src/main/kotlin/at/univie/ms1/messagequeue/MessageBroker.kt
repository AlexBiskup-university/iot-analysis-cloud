package at.univie.ms1.messagequeue

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.MessageHandler
import org.springframework.stereotype.Component

@Component
class MessageBroker(
        private val messageHandlerRegistry: MessageHandlerRegistry
) {

    fun send(message: Message) {
        val messageHandlers = messageHandlerRegistry.getMessageHandlersForTopic(message.topic)
        messageHandlers.forEach {
            it.handleMessage(message)
        }
    }

    fun subscribe(topics: List<String>, handler: MessageHandler) {
        messageHandlerRegistry.register(topics, handler)
    }

    fun unsubscribe(handler: MessageHandler) {
        messageHandlerRegistry.deregister(handler)
    }

}
