package at.univie.ms1.messagequeue.web.client

import at.univie.messagequeue.client.TerminationHandler

class WebSocketTerminationHandler(
        private val requester: ServerRequester
): TerminationHandler {

    override fun handleTermination() {
        requester.terminate()
    }

}
