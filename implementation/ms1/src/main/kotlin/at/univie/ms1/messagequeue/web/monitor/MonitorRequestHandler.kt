package at.univie.ms1.messagequeue.web.monitor

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.monitor.MonitoringSession
import at.univie.messaging.ms1.requests.monitor.ConnectRequest
import at.univie.messaging.ms1.requests.monitor.DisconnectRequest
import at.univie.messaging.ms1.requests.monitor.MonitorRequestMarshaller
import at.univie.messaging.ms1.requests.monitor.TerminateRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler
import java.util.*

@Component
class MonitorRequestHandler(
    private val marshaller: MonitorRequestMarshaller,
    private val messageQueue: MessageQueue,
    private val objectMapper: ObjectMapper,
    private val log: Logger
) : TextWebSocketHandler() {

    private val sessions: MutableMap<UUID, MonitoringSession> = mutableMapOf()

    override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        try {
            when (val request = marshaller.unmarshall(message.payload)) {
                is ConnectRequest -> connectMonitor(request, session)
                is DisconnectRequest -> disconnectMonitor(request)
                is TerminateRequest -> terminateClient(request)
            }
        } catch (exception: Throwable) {
            log.error(exception.message)
            val requester = ServerMonitorRequester(session, objectMapper)
            requester.sendError(exception.message ?: "No message available.")
        }
    }

    private fun connectMonitor(request: ConnectRequest, session: WebSocketSession) {
        log.info("Connecting: name=${request.monitorName}, id=${request.monitorId}")
        val requester = ServerMonitorRequester(session, objectMapper)
        val eventHandler = WebSocketEventHandler(requester)
        val monitor = WebSocketMonitor(id = request.monitorId, name = request.monitorName, eventHandler = eventHandler)
        val monitoringSession = messageQueue.connect(monitor)
        sessions[monitor.id] = monitoringSession
    }

    private fun disconnectMonitor(request: DisconnectRequest) {
        log.info("Disconnecting: ${request.monitorId}")
        val monitoringSession = sessions[request.monitorId]
        monitoringSession?.close()
        sessions.remove(request.monitorId)
    }

    private fun terminateClient(request: TerminateRequest) {
        log.info("Terminating: monitorId=${request.monitorId}, clientId=${request.clientId}")
        val monitoringSession = sessions[request.monitorId]
        monitoringSession?.terminate(request.clientId)
    }

}
