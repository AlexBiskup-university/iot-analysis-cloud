package at.univie.ms1.messagequeue

import at.univie.messagequeue.Message
import at.univie.messagequeue.MessageComparators
import at.univie.messagequeue.client.MessageHandler
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.time.temporal.ChronoUnit.MILLIS
import java.util.*
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS

class QueuedMessageHandler(
    private var deliveryInterval: Long,
    private val destinationHandler: MessageHandler,
    private val executor: ScheduledExecutorService,
    private val handlePublishingError: (exception: Throwable) -> Unit
) : MessageHandler {

    private val queue: PriorityQueue<Message> = PriorityQueue(MessageComparators.getDefault())
    private var nextScheduledPublishingTime: LocalDateTime = now()

    @Synchronized
    override fun handleMessage(message: Message) {
        queue.add(message)
        val delay = getDelayForNextMessage()
        val publishingTask = createPublishingTask()
        executor.schedule(publishingTask, delay, MILLISECONDS)
        setPublishingTimeForNextMessage()
    }

    private fun createPublishingTask(): () -> Unit {
        return {
            try {
                queue.poll()?.let { destinationHandler.handleMessage(it) }
            } catch (exception: Throwable) {
                handlePublishingError(exception)
            }
        }
    }

    private fun getDelayForNextMessage(): Long {
        val now = now()
        return if (now.isBefore(nextScheduledPublishingTime)) now.until(nextScheduledPublishingTime, MILLIS) else 0
    }

    private fun setPublishingTimeForNextMessage() {
        nextScheduledPublishingTime = nextScheduledPublishingTime.plus(deliveryInterval, MILLIS)
    }

    fun updateDeliveryInterval(interval: Long) {
        deliveryInterval = interval
    }
}


