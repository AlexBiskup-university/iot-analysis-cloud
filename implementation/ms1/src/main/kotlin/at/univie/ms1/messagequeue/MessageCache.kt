package at.univie.ms1.messagequeue

import at.univie.messagequeue.Message
import at.univie.messagequeue.MessageComparators
import org.springframework.stereotype.Component
import java.time.LocalDateTime.now
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.HOURS

@Component
class MessageCache(
    executor: ScheduledExecutorService
) {

    private val messages: MutableMap<String, MutableList<Message>> = mutableMapOf()

    init {
        val cleanupTask = {
            val outdatedMessages = getAll().filter { it.timestamp.isBefore(now().minusHours(1)) }
            outdatedMessages.forEach {
                messages[it.topic]?.remove(it)
            }
        }
        executor.scheduleAtFixedRate(cleanupTask, 1, 1, HOURS)
    }

    fun get(topics: List<String>): List<Message> {
        return topics.flatMap {
            messages[it]?.toList() ?: emptyList()
        }
    }

    fun getPrioritized(topics: List<String>): List<Message> {
        return get(topics).sortedWith(MessageComparators.getDefault())
    }

    fun getAll(): List<Message> {
        return messages.values.flatten()
    }

    fun cache(message: Message) {
        messages.putIfAbsent(message.topic, mutableListOf())
        messages[message.topic]!!.add(message)
    }

}
