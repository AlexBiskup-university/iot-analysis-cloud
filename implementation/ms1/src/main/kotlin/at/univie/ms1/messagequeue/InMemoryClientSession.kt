package at.univie.ms1.messagequeue

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.client.SubscriptionDetails
import at.univie.messagequeue.monitor.events.*
import at.univie.messagequeue.monitor.events.PublishEvent.MessageDetails
import org.slf4j.Logger
import java.time.LocalDateTime.now
import java.util.concurrent.ScheduledExecutorService

class InMemoryClientSession(
    private val client: Client,
    private val clientService: ClientService,
    private val messageBroker: MessageBroker,
    private val messageCache: MessageCache,
    private val eventEmitter: EventEmitter,
    private val executor: ScheduledExecutorService,
    private val log: Logger
) : ClientSession {

    private var queuedMessageHandler: QueuedMessageHandler? = null
    private val subscribed: Boolean
        get() = queuedMessageHandler != null

    override fun isOpen(): Boolean {
        return clientService.isClientPresent(client)
    }

    override fun close() {
        runWithErrorInterceptor {
            if (subscribed) unsubscribe()
            clientService.remove(client)
            eventEmitter.emit(DisconnectEvent(client.id, now()))
        }
    }

    override fun publish(message: Message) {
        runWithErrorInterceptor {
            messageBroker.send(message)
            messageCache.cache(message)
            eventEmitter.emit(PublishEvent(client.id, now(), MessageDetails(message.topic, message.priority)))
        }
    }

    override fun subscribe(details: SubscriptionDetails) {
        runWithErrorInterceptor {
            val cachedMessages = messageCache.getPrioritized(details.topics)
            val messageHandler = QueuedMessageHandler(details.deliveryInterval, details.messageHandler, executor, ::handleDestinationHandlerError)
            queuedMessageHandler = messageHandler
            cachedMessages.forEach { messageHandler.handleMessage(it) }
            messageBroker.subscribe(details.topics, messageHandler)
            eventEmitter.emit(SubscribeEvent(client.id, now(), SubscribeEvent.SubscriptionDetails(details.topics, details.deliveryInterval)))
        }
    }

    override fun unsubscribe() {
        runWithErrorInterceptor {
            queuedMessageHandler
                ?.let { messageBroker.unsubscribe(it) }
                ?.also { eventEmitter.emit(UnsubscribeEvent(client.id, now())) }
                ?: throw RuntimeException("Failed to unsubscribe message handler.")
        }
    }

    override fun updateDeliveryInterval(interval: Long) {
        runWithErrorInterceptor {
            queuedMessageHandler
                ?.also { it.updateDeliveryInterval(interval) }
                ?.also { eventEmitter.emit(UpdateSubscriptionEvent(client.id, now(), interval)) }
                ?: throw RuntimeException("Failed to update delivery interval for message handler.")
        }
    }

    private fun runWithErrorInterceptor(block: () -> Unit) {
        requireOpenSession()
        try {
            block()
        } catch (exception: Throwable) {
            eventEmitter.emit(ErrorEvent(client.id, now(), exception.message ?: "No message available."))
            throw exception
        }
    }

    private fun requireOpenSession() {
        require(isOpen()) { "Session is already closed." }
    }

    private fun handleDestinationHandlerError(exception: Throwable) {
        if (subscribed) unsubscribe()
        val message =
            "Client was forced to unsubscribe because an error occurred while trying to forward a message. Error message = ${exception.message}"
        log.error(message)
        eventEmitter.emit(ErrorEvent(clientId = client.id, timestamp = now(), message = message))
    }

}
