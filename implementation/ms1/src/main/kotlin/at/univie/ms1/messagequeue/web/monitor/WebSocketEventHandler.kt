package at.univie.ms1.messagequeue.web.monitor

import at.univie.messagequeue.monitor.EventHandler
import at.univie.messagequeue.monitor.events.Event

class WebSocketEventHandler (
        private val requester: ServerMonitorRequester
): EventHandler {

    override fun handleEvent(event: Event) {
        requester.sendEvent(event)
    }

    fun handleError(error: String) {
        requester.sendError(error)
    }

}
