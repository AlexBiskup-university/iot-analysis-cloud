package at.univie.ms1.messagequeue.web.monitor

import at.univie.messagequeue.monitor.events.Event
import at.univie.messaging.ms1.requests.monitor.MonitorServerRequest
import at.univie.messaging.ms1.requests.monitor.ReceiveErrorRequest
import at.univie.messaging.ms1.requests.monitor.ReceiveEventRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import java.time.LocalDateTime.now

class ServerMonitorRequester(
    private val session: WebSocketSession,
    private val objectMapper: ObjectMapper
) {

    fun sendEvent(event: Event) {
        sendRequest(ReceiveEventRequest(now(), event))
    }

    fun sendError(error: String) {
        sendRequest(ReceiveErrorRequest(now(), error))
    }

    private fun sendRequest(request: MonitorServerRequest) {
        val message = TextMessage(objectMapper.writeValueAsString(request))
        session.sendMessage(message)
    }
}
