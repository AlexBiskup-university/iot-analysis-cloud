package at.univie.ms1.messagequeue.web.client

import at.univie.messagequeue.Message
import at.univie.messagequeue.client.MessageHandler

class WebSocketMessageHandler(
        private val requester: ServerRequester
): MessageHandler {

    override fun handleMessage(message: Message) {
        requester.sendMessage(message)
    }

}