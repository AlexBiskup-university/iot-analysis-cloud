package at.univie.ms1.messagequeue.configuration

import at.univie.ms1.messagequeue.web.client.ClientRequestHandler
import at.univie.ms1.messagequeue.web.monitor.MonitorRequestHandler
import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.WebSocketMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator.OverflowStrategy
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator.OverflowStrategy.DROP
import org.springframework.web.socket.handler.WebSocketHandlerDecorator

@Configuration
@EnableWebSocket
class WebSocketConfiguration(
        private val clientRequestHandler: ClientRequestHandler,
        private val monitorRequestHandler: MonitorRequestHandler
) : WebSocketConfigurer {

    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(decorateHandler(clientRequestHandler), "/clients").setAllowedOrigins("*")
        registry.addHandler(decorateHandler(monitorRequestHandler), "/monitors").setAllowedOrigins("*")
    }

    private fun decorateHandler(handler: WebSocketHandler): WebSocketHandler {
        return ConcurrentWebSocketHandlerDecorator(handler, 2000, 4096, DROP)
    }

}

class ConcurrentWebSocketHandlerDecorator(
   delegate: WebSocketHandler,
   private val sendTimeLimit: Int,
   private val bufferSizeLimit: Int,
   private val overflowStrategy: OverflowStrategy
): WebSocketHandlerDecorator(delegate) {

    override fun handleTransportError(session: WebSocketSession, exception: Throwable) {
        delegate.handleTransportError(decorateSession(session), exception)
    }

    override fun afterConnectionClosed(session: WebSocketSession, closeStatus: CloseStatus) {
        delegate.afterConnectionClosed(decorateSession(session), closeStatus)
    }

    override fun handleMessage(session: WebSocketSession, message: WebSocketMessage<*>) {
        delegate.handleMessage(decorateSession(session), message)
    }

    override fun afterConnectionEstablished(session: WebSocketSession) {
        delegate.afterConnectionEstablished(decorateSession(session))
    }

    private fun decorateSession(session: WebSocketSession): WebSocketSession {
       return if(session !is ConcurrentWebSocketSessionDecorator) {
           ConcurrentWebSocketSessionDecorator(session, sendTimeLimit, bufferSizeLimit, overflowStrategy)
       } else session
    }

}
