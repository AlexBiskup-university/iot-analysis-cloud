package at.univie.ms1.messagequeue

import at.univie.messagequeue.MessageQueue
import at.univie.messagequeue.client.Client
import at.univie.messagequeue.client.ClientSession
import at.univie.messagequeue.monitor.Monitor
import at.univie.messagequeue.monitor.MonitoringSession
import at.univie.messagequeue.monitor.events.ConnectEvent
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDateTime.now
import java.util.concurrent.ScheduledExecutorService

@Service
class InMemoryMessageQueue(
    private val monitorService: MonitorService,
    private val clientService: ClientService,
    private val messageBroker: MessageBroker,
    private val messageCache: MessageCache,
    private val executorService: ScheduledExecutorService
) : MessageQueue {

    override fun connect(client: Client): ClientSession {
        clientService.save(client)
        monitorService.emit(ConnectEvent(client.id, now(), client.name))
        val log = LoggerFactory.getLogger(InMemoryClientSession::class.java)
        return InMemoryClientSession(client, clientService, messageBroker, messageCache, monitorService, executorService, log)
    }

    override fun connect(monitor: Monitor): MonitoringSession {
        val log = LoggerFactory.getLogger(InMemoryMonitoringSession::class.java)
        return InMemoryMonitoringSession(monitor, monitorService, clientService, log)
    }
}
