package at.univie.ms2cliclient

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["at.univie"])
@EnableConfigurationProperties(MonitorServerProperties::class)
class Ms2CliClientApplication

fun main(args: Array<String>) {
    runApplication<Ms2CliClientApplication>(*args)
}
