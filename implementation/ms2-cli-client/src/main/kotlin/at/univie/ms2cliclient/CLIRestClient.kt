package at.univie.ms2cliclient

import at.univie.messaging.monitoring.ClientDetails
import at.univie.messaging.monitoring.MonitoringData
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.net.URL
import java.util.*


@Service
class CLIRestClient(
    monitorServerProperties: MonitorServerProperties,
    private val restTemplate: RestTemplate
) {

    val baseURL = "${monitorServerProperties.url}/monitoring"

    fun getExchanged(): Int {
        return URL("$baseURL/exchanged").readText().toInt()
    }

    fun getErrors(): Int {
        return URL("$baseURL/errors").readText().toInt()
    }

    fun getConnectedClients(): List<ClientDetails> {
        return restTemplate.getForObject("$baseURL/clients", Array<ClientDetails>::class.java)!!.toList()
    }

    fun getMonitoringData(): MonitoringData {
        return restTemplate.getForObject(baseURL, MonitoringData::class.java)!!
    }

    fun terminateClient(id: UUID) {
        restTemplate.postForObject("$baseURL/terminate", id, Unit::class.java)
    }

}
