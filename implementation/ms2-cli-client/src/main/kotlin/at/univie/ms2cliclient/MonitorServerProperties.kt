package at.univie.ms2cliclient

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "monitor-server")
class MonitorServerProperties (
    val url: String
)
