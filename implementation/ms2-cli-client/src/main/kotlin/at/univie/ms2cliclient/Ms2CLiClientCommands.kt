package at.univie.ms2cliclient

import at.univie.messaging.monitoring.MonitoringData
import org.springframework.shell.standard.ShellCommandGroup
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import java.util.*

@ShellComponent
@ShellCommandGroup("CLI Client Commands")
class Ms2CliClientCommands(val restClient: CLIRestClient) {

    @ShellMethod(key = ["messages"], value = "Display number of exchanged messages.")
    fun displayExchanged(): String {
        return "Number of exchanged messages ${restClient.getExchanged()}."
    }

    @ShellMethod(key = ["errors"], value = "Display number of observed errors.")
    fun displayErrors(): String {
        return "Number of observed errors ${restClient.getErrors()}."
    }

    @ShellMethod(key = ["clients"], value = "Display connected clients.")
    fun displayConnectedClients(): Any {
        val clients = restClient.getConnectedClients()
        return if (clients.isNotEmpty()) clients else "No clients are currently connected."
    }

    @ShellMethod(key = ["all"], value = "Display all available information.")
    fun displayMonitoringData(): MonitoringData {
        return restClient.getMonitoringData()
    }

    @ShellMethod("Terminate client with a given UID.")
    fun terminateClient(id: UUID) {
        restClient.terminateClient(id)
    }
}
