package at.univie.ms2cliclient

import at.univie.messaging.monitoring.ClientDetails
import at.univie.messaging.monitoring.MonitoringData
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.mockito.Mockito
import java.time.LocalDateTime
import java.util.*

internal class Ms2CliClientCommandsTest {
    private val cliRestClient = Mockito.mock(CLIRestClient::class.java)
    private val commands = Ms2CliClientCommands(cliRestClient)

    @Test
    fun displayExchanged() {
        Mockito.`when`(cliRestClient.getExchanged()).thenReturn(3)
        val exchanged = commands.displayExchanged()
        assertEquals("Number of exchanged messages 3.", exchanged)
    }

    @Test
    fun displayErrors() {
        Mockito.`when`(cliRestClient.getErrors()).thenReturn(3)
        val errors = commands.displayErrors()
        assertEquals("Number of observed errors 3.", errors)
    }

    @Test
    fun displayConnectedClients() {
        Mockito.`when`(cliRestClient.getConnectedClients()).thenReturn(
                listOf(ClientDetails("1", UUID.randomUUID(), LocalDateTime.now()))
        )
        val connected = commands.displayConnectedClients() as List<ClientDetails>
        assertEquals(1, connected.size)
        assertEquals("1", connected[0].name)
    }

    @Test
    fun displayMonitoringData() {
        Mockito.`when`(cliRestClient.getMonitoringData()).thenReturn(
                MonitoringData(3, 3, listOf())
        )
        val monitoringData = commands.displayMonitoringData()
        assertEquals(3, monitoringData.errors)
    }

    @Test
    fun terminateClient() {
        val id = UUID.randomUUID()
        commands.terminateClient(id)
        Mockito.verify(cliRestClient).terminateClient(id)
    }
}